/**
 *******************************************************************************
 *
 * @file gadc.h
 *
 * @brief Analog-to-digital converter
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */

#pragma once

/**
 *******************************************************************************
 * @defgroup GADC General purpose ADC
 * @ingroup DRIVERS
 * @brief User driver for General purpose Analog to Digital Converter
 *
 * @{
 *******************************************************************************
 */


#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__IEEE_LITTLE_ENDIAN) || defined(__LITTLE_ENDIAN__)
/// Calibration data type
struct gadc_cal_s {
    union {
	/// 32 bits data which combine offset and gain
	uint32_t value;
	/// Decomposition structure.
	struct {
	    /// Mantissa part of gain.
	    unsigned int c1_mantissa:12;
	    /// Exponent part of gain.
	    int c1_exponent:6;
	    /// Sign part of gain.
	    unsigned int c1_sign:1;
	    /// Double value of offset
	    int c0_x2:13;
	};
    };
};

STATIC_ASSERT(sizeof(struct gadc_cal_s) == 4, "wrong size");
/// Float data type
typedef union {
    /// C float value
    float value;
    /// Decomposition structure.
    struct {
	/// Mantissa part of float.
	unsigned int fraction: 23;
	/// Exponent part of float.
	int exponent: 8;
	/// Sign part of float.
	unsigned int sign : 1;
    } number;
} __ieee_float_shape_type;
#else
#error "Unsupported floating point endian"
#endif

/// Channels used by GADC
typedef enum {
    /// VBAT channel.
    VBATT =  0,
    /// VSTORE channel.
    VSTORE = 1,
    /// VDD1A channel.
    CORE = 2,
    /// Temperature channel.
    TEMP = 3,
    /// P10/P11 differential channel.
    PORT0_DIFFERENTIAL = 4,
    /// P10 single-ended channel.
    PORT0_SINGLE_ENDED_0 = 6,
    /// P11 single-ended channel.
    PORT0_SINGLE_ENDED_1 = 7,
    /// For GADC driver use only
    ZV_PORT = 8,
    /// P9 single-ended channel.
    PORT1_SINGLE_ENDED_1 = 9,
} gadc_chan_t;

/// Callback context data
typedef struct {
    /// Sensed channel.
    gadc_chan_t ch;
    /// Gext setting. (0 = more range / 1 = better resolution)
    uint8_t gext;
    /// Application context.
    void const *app_ctx;
} gadc_cb_ctx_t;

/// Callback prototype
typedef void (*gadc_callback_t)(float result, int16_t raw, struct gadc_cal_s cal,
    gadc_cb_ctx_t const *ctx);

/**
 * @brief Sample given GADC channel and invoke callback on completion of measurement
 * @param[in] channel    GADC channel to monitor
 * @param[in] cb  Callback invoked with channel measurement on completion.
 * @param[in] gext gain setting
 * @param[in] ctx Context associated.
 */
void gadc_sample_channel(gadc_chan_t channel, gadc_callback_t cb, uint8_t gext,
    void const *ctx);

#ifdef __cplusplus
}
#endif

/// @} GADC

