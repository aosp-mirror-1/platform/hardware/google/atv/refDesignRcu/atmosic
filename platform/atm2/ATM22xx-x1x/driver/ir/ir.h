/**
 *******************************************************************************
 *
 * @file ir.h
 *
 * @brief IR driver interface
 *
 * Copyright (C) Atmosic 2019-2022
 *
 *******************************************************************************
 */

#pragma once

/**
 * @defgroup IR IR
 * @ingroup DRIVERS
 * @brief Driver for IR module
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Reset IR sequence.
 *
 */
void reset_ir_sequence(void);

/**
 * @brief Send IR sequence.
 *
 */
void send_ir_sequence(void);

/**
 * @brief add IR period in period_us and carrier_on or off flag
 * @param[in] carrier_on true if carrier freq needs to be used during the period
 * @param[in] period_us Period in us
 */
void ir_add_period(uint8_t carrier_on, uint32_t period_us);

/**
 * @brief Initialize IR subsystem with given protocol
 * @param[in] callback Callback to receive IR sequence completion event.
 * @note This callback is invoked in interrupt context.
 */
void ir_init(void (*callback)(void));

/**
 * @brief Configure IR carrier parameters
 * @param[in] freq Carrier frequency setting in Hz
 * @param[in] duty Carrier duty cycle setting in percentage
 */
void ir_config_carr(uint32_t freq, uint8_t duty);

#ifdef __cplusplus
}
#endif

/// @} IR
