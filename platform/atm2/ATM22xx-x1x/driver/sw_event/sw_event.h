/**
 ******************************************************************************
 *
 * @file sw_event.h
 *
 * @brief Atmosic Software Event Driver
 *
 * Copyright (C) Atmosic 2020
 *
 ******************************************************************************
 */

#ifndef __SW_EVENT_H__
#define __SW_EVENT_H__

/**
 * @defgroup SW_EVENT Software Event APIs
 * @ingroup DRIVERS
 * @brief User driver for application to use software events
 * @{
 */

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// Maximum number of concurrent events - adjust as needed
#define SW_EVENT_ID_MAX	8

/// Event ID type
typedef uint8_t sw_event_id_t;

/// Event triggered callback function type
typedef void (*sw_event_func_t)(sw_event_id_t event_id, const void *ctx);

/**
 * @brief Allocate and configure event
 * @param[in] handler  Event triggered callback (called from main event loop)
 * @param[in] ctx      Context to pass to handler
 * @return             Event ID
 */
sw_event_id_t sw_event_alloc(sw_event_func_t handler, const void *ctx);

/**
 * @brief Deallocate event
 * @note Caller must guarantee that set/clear methods are not invoked from
 *       an ISR while this method is running
 * @param[in] event_id  Event ID from sw_event_alloc()
 */
void sw_event_free(sw_event_id_t event_id);

/**
 * @brief Reconfigure event handler and context
 * @param[in] event_id  Event ID from sw_event_alloc()
 * @param[in] handler   Event triggered callback (called from main event loop)
 * @param[in] ctx       Context to pass to handler
 */
void sw_event_reconfig(sw_event_id_t event_id, sw_event_func_t handler,
    const void *ctx);

/**
 * @brief Trigger event
 * @note Safe to be called from ISR
 * @param[in] event_id  Event ID from sw_event_alloc()
 */
void sw_event_set(sw_event_id_t event_id);

/**
 * @brief Clear event
 * @note Safe to be called from ISR
 * @param[in] event_id  Event ID from sw_event_alloc()
 */
void sw_event_clear(sw_event_id_t event_id);

/**
 * @brief Get event status
 * @param[in] event_id  Event ID from sw_event_alloc()
 * @return              True when event is valid and triggered
 */
bool sw_event_get(sw_event_id_t event_id);

#ifdef __cplusplus
}
#endif

/// @} SW_EVENT

#endif // __SW_EVENT_H__
