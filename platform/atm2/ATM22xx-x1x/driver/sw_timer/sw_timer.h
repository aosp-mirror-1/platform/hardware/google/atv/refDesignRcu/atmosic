/**
 ******************************************************************************
 *
 * @file sw_timer.h
 *
 * @brief Atmosic Software Timer Driver
 *
 * Copyright (C) Atmosic 2020-2021
 *
 ******************************************************************************
 */

#ifndef __SW_TIMER_H__
#define __SW_TIMER_H__

/**
 * @defgroup SW_TIMER Software Timer APIs
 * @ingroup DRIVERS
 * @brief User driver for application to use software timers
 * @{
 */

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// Maximum number of concurrent timers - adjust as needed
#ifndef SW_TIMER_ID_MAX
#define SW_TIMER_ID_MAX 8
#endif // SW_TIMER_ID_MAX

/// Smallest granularity for sw_timer_set()
#define SW_TIMER_10_MS 1
/// Commonly used quantity for sw_timer_set()
#define SW_TIMER_1_SEC 100

/// Timer ID type
typedef uint8_t sw_timer_id_t;

/// Timer expired callback function type
typedef void (*sw_timer_func_t)(sw_timer_id_t timer_id, const void *ctx);

/**
 * @brief Allocate and configure timer
 * @param[in] handler  Timer expired callback (called from main event loop)
 * @param[in] ctx      Context to pass to handler
 * @return             Timer ID
 */
sw_timer_id_t sw_timer_alloc(sw_timer_func_t handler, const void *ctx);

/**
 * @brief Deallocate timer
 * @param[in] timer_id  Timer ID from sw_timer_alloc()
 */
void sw_timer_free(sw_timer_id_t timer_id);

/**
 * @brief Reconfigure timer handler and context
 * @param[in] timer_id  Timer ID from sw_timer_alloc()
 * @param[in] handler   Timer expired callback (called from main event loop)
 * @param[in] ctx       Context to pass to handler
 */
void sw_timer_reconfig(sw_timer_id_t timer_id, sw_timer_func_t handler,
    const void *ctx);

/**
 * @brief Start one-shot timer running
 * @param[in] timer_id  Timer ID from sw_timer_alloc()
 * @param[in] centisec  Duration in hundredths of seconds
 */
void sw_timer_set(sw_timer_id_t timer_id, uint32_t centisec);

/**
 * @brief Stop/abort running timer
 * @param[in] timer_id  Timer ID from sw_timer_alloc()
 */
void sw_timer_clear(sw_timer_id_t timer_id);

/**
 * @brief Get timer status
 * @param[in] timer_id  Timer ID from sw_timer_alloc()
 * @return              True when timer is valid and running
 */
bool sw_timer_active(sw_timer_id_t timer_id);

#ifdef __cplusplus
}
#endif

/// @} SW_TIMER

#endif // __SW_TIMER_H__
