/**
 *******************************************************************************
 *
 * @file led_blink.h
 *
 * @brief LED control
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup LED LED driver
 * @ingroup DRIVERS
 * @brief ATM bluetooth framework LED driver
 *
 * This module contains the necessary function of LED.
 *
 * @{
 *******************************************************************************
 */

#ifdef __cplusplus
extern "C" {
#endif

/// LED ID type
typedef uint8_t led_id_t;

/// LED enum
enum {
    /// LED_0
    LED_0,
    /// LED_1
    LED_1,

    /// LED_MAX
    LED_MAX
};

/**
 *******************************************************************************
 * @brief Config LED blink parameter
 * @param[in] id LED ID
 * @param[in] hi_dur LED hi activity time(unit: 10ms)
 * @param[in] low_dur LED low activity time(unit: 10ms)
 * @param[in] times The count for Hi+Low LED activity
 *******************************************************************************
 */
void led_blink(led_id_t id, uint16_t hi_dur, uint16_t low_dur, uint16_t times);

/**
 *******************************************************************************
 * @brief Turn on LED
 * @param[in] id LED ID
 *******************************************************************************
 */
void led_on(led_id_t id);

/**
 *******************************************************************************
 * @brief Turn off LED
 * @param[in] id LED ID
 *******************************************************************************
 */
void led_off(led_id_t id);

/**
 *******************************************************************************
 * @brief Get LED status
 * @param[in] id LED ID
 * @return LED pin value.
 *******************************************************************************
 */
bool led_status(led_id_t id);

#ifdef __cplusplus
}
#endif

///@} LED
