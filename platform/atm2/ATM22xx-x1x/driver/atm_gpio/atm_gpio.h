/*
 *-----------------------------------------------------------------------------
 * The confidential and proprietary information contained in this file may
 * only be used by a person authorised under and to the extent permitted
 * by a subsisting licensing agreement from ARM Limited.
 *
 *            (C) COPYRIGHT 2010-2013 ARM Limited.
 *                ALL RIGHTS RESERVED
 *
 * This entire notice must be reproduced on all copies of this file
 * and copies of this file may only be made by a person if such person is
 * permitted to do so under the terms of a subsisting license agreement
 * from ARM Limited.
 *
 *      SVN Information
 *
 *      Checked In          : $Date: 2012-05-28 18:02:18 +0100 (Mon, 28 May 2012) $
 *
 *      Revision            : $Revision: 210377 $
 *
 *      Release Information : Cortex-M System Design Kit-r1p0-00rel0
 *-----------------------------------------------------------------------------
 */
/*************************************************************************
 * @file     atm_gpio.h
 * @brief    ATM GPIO Device Driver Header File
 * Copyright (C) Atmosic 2019-2020
 ******************************************************************************/

/** @addtogroup ATM_GPIO_Driver_definitions
  This file defines Atmosic GPIO Driver functions.
  @{
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/// The atm_gpio error code
typedef enum atm_gpio_err_s {
    /// No Error
    ATM_GPIO_NO_ERR,
    /// GPIO invalid
    ATM_GPIO_ERR_INVALID,
    /// GPIO unconfigured
    ATM_GPIO_ERR_UNCFG
} atm_gpio_err_t;

  /**
   * @brief Sets up internal config for use as GPIO
   * This function needs to be called prior to any GPIO configuration.
   * @param[in] gpio pin number
   */

 void atm_gpio_setup(uint8_t gpio);

  /**
   * @brief Set GPIO PIN as input.
   * @param[in] gpio pin number
   */

 void atm_gpio_set_input(uint8_t gpio);

  /**
   * @brief Clear GPIO PIN as input.
   * @param[in] gpio pin number
   */

 void atm_gpio_clear_input(uint8_t gpio);

  /**
   * @brief Set GPIO PIN as output.
   * @param[in] gpio pin number
   */

 void atm_gpio_set_output(uint8_t gpio);

  /**
   * @brief Clear GPIO PIN as output.
   * @param[in] gpio pin number
   */

 void atm_gpio_clear_output(uint8_t gpio);

  /**
   * @brief Set GPIO PIN pullup.
   * @param[in] gpio pin number
   */

 void atm_gpio_set_pullup(uint8_t gpio);

  /**
   * @brief Clear GPIO PIN pullup.
   * @param[in] gpio pin number
   */

 void atm_gpio_clear_pullup(uint8_t gpio);

  /**
   * @brief Read GPIO Interrupt Status.
   * @param[in] gpio pin number
   */

 bool atm_gpio_read_int_status(uint8_t gpio);

  /**
   * @brief Clear GPIO PIN Interrupt Status.
   * @param[in] gpio pin number
   */

 void atm_gpio_clear_int_status(uint8_t gpio);

  /**
   * @brief Enable GPIO PIN Interrupt.
   * @param[in] gpio pin number
   */

 void atm_gpio_set_int_enable(uint8_t gpio);

  /**
   * @brief Disable GPIO PIN Interrupt.
   * @param[in] gpio pin number
   */

 void atm_gpio_set_int_disable(uint8_t gpio);

  /**
   * @brief Setup GPIO PIN Interrupt as high level.
   * @param[in] gpio pin number
   */

 void atm_gpio_int_set_high(uint8_t gpio);

  /**
   * @brief Setup GPIO PIN Interrupt as rising edge.
   * @param[in] gpio pin number
   */

 void atm_gpio_int_set_rising(uint8_t gpio);

  /**
   * @brief Setup GPIO PIN Interrupt as low level.
   * @param[in] gpio pin number
   */

 void atm_gpio_int_set_low(uint8_t gpio);

  /**
   * @brief Setup GPIO PIN Interrupt as falling edge.
   * @param[in] gpio pin number
   */

 void atm_gpio_int_set_falling(uint8_t gpio);

  /**
   * @brief Setup GPIO PIN Interrupt as high or low level.
   * @param[in] gpio pin number
   * @return Previous Interrupt edge configure.
   */

 bool atm_gpio_toggle_int_edge(uint8_t gpio);

  /**
   * @brief Write GPIO PIN output (1: high, 0: low)
   * @param[in] gpio pin number
   * @param[in] value (0/1)
   */

 void atm_gpio_write(uint8_t gpio, bool value);

  /**
   * @brief Read GPIO PIN.
   * @param[in] gpio pin number
   */

 bool atm_gpio_read_gpio(uint8_t gpio);

  /**
   * @brief Toggle GPIO PIN.
   * @param[in] gpio pin number
   * @return GPIO pin value after toggling.
   */
 bool atm_gpio_toggle(uint8_t gpio);

  /**
   * @brief GPIO pin validation.
   * @param[in] gpio pin number
   * @return Error code (@ref atm_gpio_err_t)
   */
 uint8_t atm_gpio_validate_gpio(uint8_t gpio);

#ifdef __cplusplus
}
#endif

  /*@}*/ /* end of group ATM_GPIO_Driver_definitions Atmosic GPIO Driver definitions */
