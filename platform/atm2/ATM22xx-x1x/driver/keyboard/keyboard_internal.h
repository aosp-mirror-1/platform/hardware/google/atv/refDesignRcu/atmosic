/**
 *******************************************************************************
 *
 * @file keyboard_internal.h
 *
 * @brief keyboard ini helper macro
 * This file is only included by keyboard_init() to reduce the context length.
 *
 * Copyright (C) Atmosic 2018-2021
 *
 *******************************************************************************
 */
#ifdef __KEYBOARD_INIT_USAGE__

#ifdef __cplusplus
extern "C" {
#endif

// Macro for setting mux
#define KSM_COL_SET(x) do { \
    if (!(ksm_pin_check & (1ULL << PIN_COL##x))) { \
	DEBUG_TRACE("P%d is not supported.", PIN_COL##x); \
	ASSERT_ERR(0); \
    } \
    PINMUX_KSO_SET(x); \
    col[COL##x##_KSO] = x; \
} while (0)

#define KSM_ROW_SET(x) do { \
    if (!(ksm_pin_check & (1ULL << PIN_ROW##x))) { \
	DEBUG_TRACE("P%d is not supported.", PIN_ROW##x); \
	ASSERT_ERR(0); \
    } \
    PINMUX_KSI_SET(x); \
    row[ROW##x##_KSI] = x; \
} while (0)

#if (MAX_ROW > 0) && defined(PIN_ROW0) && defined(ROW0_KSI)
    KSM_ROW_SET(0);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW0);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 0) && defined(PIN_ROW0) && defined(ROW0_KSI)

#if (MAX_ROW > 1) && defined(PIN_ROW1) && defined(ROW1_KSI)
    KSM_ROW_SET(1);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW1);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 1) && defined(PIN_ROW1) && defined(ROW1_KSI)

#if (MAX_ROW > 2) && defined(PIN_ROW2) && defined(ROW2_KSI)
    KSM_ROW_SET(2);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW2);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 2) && defined(PIN_ROW2) && defined(ROW2_KSI)

#if (MAX_ROW > 3) && defined(PIN_ROW3) && defined(ROW3_KSI)
    KSM_ROW_SET(3);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW3);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 3) && defined(PIN_ROW3) && defined(ROW3_KSI)

#if (MAX_ROW > 4) && defined(PIN_ROW4) && defined(ROW4_KSI)
    KSM_ROW_SET(4);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW4);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 4) && defined(PIN_ROW4) && defined(ROW4_KSI)

#if (MAX_ROW > 5) && defined(PIN_ROW5) && defined(ROW5_KSI)
    KSM_ROW_SET(5);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW5);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 5) && defined(PIN_ROW5) && defined(ROW5_KSI)

#if (MAX_ROW > 6) && defined(PIN_ROW6) && defined(ROW6_KSI)
    KSM_ROW_SET(6);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW6);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 6) && defined(PIN_ROW6) && defined(ROW6_KSI)

#if (MAX_ROW > 7) && defined(PIN_ROW7) && defined(ROW7_KSI)
    KSM_ROW_SET(7);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW7);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 7) && defined(PIN_ROW7) && defined(ROW7_KSI)

#if (MAX_ROW > 8) && defined(PIN_ROW8) && defined(ROW8_KSI)
    KSM_ROW_SET(8);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW8);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 8) && defined(PIN_ROW8) && defined(ROW8_KSI)

#if (MAX_ROW > 9) && defined(PIN_ROW9) && defined(ROW9_KSI)
    KSM_ROW_SET(9);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW9);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 9) && defined(PIN_ROW9) && defined(ROW9_KSI)

#if (MAX_ROW > 10) && defined(PIN_ROW10) && defined(ROW10_KSI)
    KSM_ROW_SET(10);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW10);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 10) && defined(PIN_ROW10) && defined(ROW10_KSI)

#if (MAX_ROW > 11) && defined(PIN_ROW11) && defined(ROW11_KSI)
    KSM_ROW_SET(11);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW11);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 11) && defined(PIN_ROW11) && defined(ROW11_KSI)

#if (MAX_ROW > 12) && defined(PIN_ROW12) && defined(ROW12_KSI)
    KSM_ROW_SET(12);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW12);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 12) && defined(PIN_ROW12) && defined(ROW12_KSI)

#if (MAX_ROW > 13) && defined(PIN_ROW13) && defined(ROW13_KSI)
    KSM_ROW_SET(13);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW13);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 13) && defined(PIN_ROW13) && defined(ROW13_KSI)

#if (MAX_ROW > 14) && defined(PIN_ROW14) && defined(ROW14_KSI)
    KSM_ROW_SET(14);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW14);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 14) && defined(PIN_ROW14) && defined(ROW14_KSI)

#if (MAX_ROW > 15) && defined(PIN_ROW15) && defined(ROW15_KSI)
    KSM_ROW_SET(15);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW15);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 15) && defined(PIN_ROW15) && defined(ROW15_KSI)

#if (MAX_ROW > 16) && defined(PIN_ROW16) && defined(ROW16_KSI)
    KSM_ROW_SET(16);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW16);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 16) && defined(PIN_ROW16) && defined(ROW16_KSI)

#if (MAX_ROW > 17) && defined(PIN_ROW17) && defined(ROW17_KSI)
    KSM_ROW_SET(17);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW17);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 17) && defined(PIN_ROW17) && defined(ROW17_KSI)

#if (MAX_ROW > 18) && defined(PIN_ROW18) && defined(ROW18_KSI)
    KSM_ROW_SET(18);
#ifdef KSI_PULLUPS
    PIN_PULLUP(PIN_ROW18);
#endif // KSI_PULLUPS
#endif // (MAX_ROW > 18) && defined(PIN_ROW18) && defined(ROW18_KSI)

#if (MAX_COL > 0) && defined(PIN_COL0) && defined(COL0_KSO)
    KSM_COL_SET(0);
#endif // (MAX_COL > 0) && defined(PIN_COL0) && defined(COL0_KSO)

#if (MAX_COL > 1) && defined(PIN_COL1) && defined(COL1_KSO)
    KSM_COL_SET(1);
#endif // (MAX_COL > 1) && defined(PIN_COL1) && defined(COL1_KSO)

#if (MAX_COL > 2) && defined(PIN_COL2) && defined(COL2_KSO)
    KSM_COL_SET(2);
#endif // (MAX_COL > 2) && defined(PIN_COL2) && defined(COL2_KSO)

#if (MAX_COL > 3) && defined(PIN_COL3) && defined(COL3_KSO)
    KSM_COL_SET(3);
#endif // (MAX_COL > 3) && defined(PIN_COL3) && defined(COL3_KSO)

#if (MAX_COL > 4) && defined(PIN_COL4) && defined(COL4_KSO)
    KSM_COL_SET(4);
#endif // (MAX_COL > 4) && defined(PIN_COL4) && defined(COL4_KSO)

#if (MAX_COL > 5) && defined(PIN_COL5) && defined(COL5_KSO)
    KSM_COL_SET(5);
#endif // (MAX_COL > 5) && defined(PIN_COL5) && defined(COL5_KSO)

#if (MAX_COL > 6) && defined(PIN_COL6) && defined(COL6_KSO)
    KSM_COL_SET(6);
#endif // (MAX_COL > 6) && defined(PIN_COL6) && defined(COL6_KSO)

#if (MAX_COL > 7) && defined(PIN_COL7) && defined(COL7_KSO)
    KSM_COL_SET(7);
#endif // (MAX_COL > 7) && defined(PIN_COL7) && defined(COL7_KSO)

#if (MAX_COL > 8) && defined(PIN_COL8) && defined(COL8_KSO)
    KSM_COL_SET(8);
#endif // (MAX_COL > 8) && defined(PIN_COL8) && defined(COL8_KSO)

#if (MAX_COL > 9) && defined(PIN_COL9) && defined(COL9_KSO)
    KSM_COL_SET(9);
/// In hw_cfg_pseq_init, the P1 was pullup to avoid leakage on designs
/// that tie TMC high. But if P1 is used for column of keymatrix, pullup will
/// cause current leakage when keyscan. So here clears the pull up for P1.
#if (PIN_COL9 == 1)
    PIN_PULL_CLR(PIN_COL9);
#endif
#endif // (MAX_COL > 9) && defined(PIN_COL9) && defined(COL9_KSO)

#if (MAX_COL > 10) && defined(PIN_COL10) && defined(COL10_KSO)
    KSM_COL_SET(10);
#endif // (MAX_COL > 10) && defined(PIN_COL10) && defined(COL10_KSO)

#if (MAX_COL > 11) && defined(PIN_COL11) && defined(COL11_KSO)
    KSM_COL_SET(11);
#endif // (MAX_COL > 11) && defined(PIN_COL11) && defined(COL11_KSO)

#if (MAX_COL > 12) && defined(PIN_COL12) && defined(COL12_KSO)
    KSM_COL_SET(12);
#endif // (MAX_COL > 12) && defined(PIN_COL12) && defined(COL12_KSO)

#if (MAX_COL > 13) && defined(PIN_COL13) && defined(COL13_KSO)
    KSM_COL_SET(13);
#endif // (MAX_COL > 13) && defined(PIN_COL13) && defined(COL13_KSO)

#if (MAX_COL > 14) && defined(PIN_COL14) && defined(COL14_KSO)
    KSM_COL_SET(14);
#endif // (MAX_COL > 14) && defined(PIN_COL14) && defined(COL14_KSO)

#if (MAX_COL > 15) && defined(PIN_COL15) && defined(COL15_KSO)
    KSM_COL_SET(15);
#endif // (MAX_COL > 15) && defined(PIN_COL15) && defined(COL15_KSO)

#if (MAX_COL > 16) && defined(PIN_COL16) && defined(COL16_KSO)
    KSM_COL_SET(16);
#endif // (MAX_COL > 16) && defined(PIN_COL16) && defined(COL16_KSO)

#if (MAX_COL > 17) && defined(PIN_COL17) && defined(COL17_KSO)
    KSM_COL_SET(17);
#endif // (MAX_COL > 17) && defined(PIN_COL17) && defined(COL17_KSO)

#if (MAX_COL > 18) && defined(PIN_COL18) && defined(COL18_KSO)
    KSM_COL_SET(18);
#endif // (MAX_COL > 18) && defined(PIN_COL18) && defined(COL18_KSO)

#if (MAX_COL > 19) && defined(PIN_COL19) && defined(COL19_KSO)
    KSM_COL_SET(19);
#endif // (MAX_COL > 19) && defined(PIN_COL19) && defined(COL19_KSO)

#ifdef __cplusplus
}
#endif

#undef __KEYBOARD_INIT_USAGE__
#endif // __KEYBOARD_INIT_USAGE__
