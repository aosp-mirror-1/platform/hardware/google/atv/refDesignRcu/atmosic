/*
 * This file should be auto-generated using a tool that consumes the pin
 * selections in the JSON file
 */

#define PIN_PDM_POWER_SWITCH 22
#define PIN_LPCOMP_IO 11
#define PIN_LED0 27
#define PIN_LED1 28
#define PIN_IR_IO 26
#ifdef CFG_ATVRC_FIND_ME
#define PIN_BUZZER_IO 26 // IR LED
// #define PIN_BUZZER_IO 27 // GREEN LED
#endif
#define PIN_ROW0 23
#define PIN_ROW1 22
#define PIN_ROW2 21
#define PIN_ROW3 13
#define PIN_ROW4 6
#define PIN_COL0 29
#define PIN_COL1 28
#define PIN_COL2 9
#define PIN_COL3 8
#define PIN_COL4 7
#define ROW0_KSI 0
#define ROW1_KSI 1
#define ROW2_KSI 2
#define ROW3_KSI 6
#define ROW4_KSI 13
#define COL0_KSO 2
#define COL1_KSO 3
#define COL2_KSO 10
#define COL3_KSO 11
#define COL4_KSO 12
