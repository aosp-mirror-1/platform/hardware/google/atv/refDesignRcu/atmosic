/**
 *******************************************************************************
 *
 * @file HID_Remote.c
 *
 * @brief HID remote controller
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */
#include "arch.h"
#include "rc_mmi.h"

static rep_vec_err_t rc_init(void)
{
    rc_mmi_init();

    return RV_DONE;
}

int main(void)
{
    RV_APPM_INIT_ADD_LAST(rc_init);

    return 0;
}
