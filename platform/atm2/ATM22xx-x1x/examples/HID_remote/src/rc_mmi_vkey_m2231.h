/**
 *******************************************************************************
 *
 * @file rc_mmi_vkey_m2231.h
 *
 * @brief Key definitions for ATV Ref. Remote G10
 *
 * @note This file should only be included by rc_mmi_vkey.c
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

// Virtual key definitions
enum {
    VK_INPUT, VK_POWER, VK_DOWN, VK_VOLUP, VK_BACK,
    VK_UP, VK_BKMK, NONE1, VK_HOME, VK_LEFT,
    VK_DASHB, VK_ASST, VK_APP03, VK_YOUTUBE, VK_VOLDN,
    NONE2, VK_NETFLIX, VK_APP04, VK_MUTE, VK_CENTER,
    NONE3, VK_CNLD, VK_GUIDE, VK_CNLU, VK_RIGHT,
};

static uint32_t __ATM_VKEY_MAP_CONST bt_keycode[] = {
    BT_INPUT, BT_POWER, BT_DOWN, BT_VOLU, BT_BACK,
    BT_UP, BT_BKMK, 0, BT_HOME, BT_LEFT,
    BT_DASHB, BT_ASST, BT_APP03, BT_YOUTUBE, BT_VOLD,
    0, BT_NETFLIX, BT_APP04, BT_MUTE, BT_CENTER,
    0, BT_CNLD, BT_GUIDE, BT_CNLU, BT_RIGHT,
};

#ifdef CFG_ATVRC_UNI_IR
static uint32_t const atv_keycode[] = {
    ATV_INPUT, ATV_POWER, 0, ATV_VOLU, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, ATV_VOLD,
    0, 0, 0, ATV_MUTE, 0,
    0, 0, 0, 0, 0,
};
#endif

#ifdef CFG_RC_IR
static uint32_t __ATM_VKEY_MAP_CONST ir_addr[] = {
    IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR,
    IR_ADDR, IR_ADDR, 0, IR_ADDR, IR_ADDR,
    IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR,
    0, IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR,
    0, IR_ADDR, IR_ADDR, IR_ADDR, IR_ADDR,
};

static uint32_t __ATM_VKEY_MAP_CONST ir_cmd[] = {
    IR_INPUT, IR_POWER, IR_DOWN, IR_VOLU, IR_BACK,
    IR_UP, IR_BKMK, 0, IR_HOME, IR_LEFT,
    IR_DASHB, IR_ASST, IR_APP03, IR_YOUTUBE, IR_VOLD,
    0, IR_NETFLIX, IR_APP04, IR_MUTE, IR_CENTER,
    0, IR_CNLD, IR_GUIDE, IR_CNLU, IR_RIGHT,
};
#endif
