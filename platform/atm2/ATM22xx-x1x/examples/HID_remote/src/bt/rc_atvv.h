/**
 *******************************************************************************
 *
 * @file rc_atvv.h
 *
 * @brief ATVV application part
 *
 * Copyright (C) Atmosic 2021
 *
 *******************************************************************************
 */
#pragma once

#ifdef CFG_ATVRC_AUDIO
void rc_atvv_ready(uint8_t ready);

void rc_atvv_mic_open(bool is8k);

void rc_atvv_mic_close(void);

uint16_t rc_atvv_get_pred_val(uint8_t *idx);
#else
#include "ble_atvvs.h"

/**
 *******************************************************************************
 * @brief Get ATVV profile parameter.
 * This is used for passing parameters to @ref atm_gap_prf_reg.
 * @return Parameters.
 *******************************************************************************
 */
ble_atvvs_param_t const *rc_atvv_param(void);
#endif

/**
 *******************************************************************************
 * @brief Start ATVV search.
 *******************************************************************************
 */
void rc_atvv_start_search(void);

/**
 *******************************************************************************
 * @brief DPAD select to ATVV client.
 *******************************************************************************
 */
void rc_atvv_dpad_select(void);

/**
 *******************************************************************************
 * @brief Stop ATVV search.
 *******************************************************************************
 */
void rc_atvv_stop_search(void);

/**
 *******************************************************************************
 * @brief provide PCM data
 * @param[in] pcm PCM data.
 *******************************************************************************
 */
void rc_atvv_fill_pcm(uint8_t pcm);

#ifndef CFG_ATVRC_AUDIO
/**
 *******************************************************************************
 * @brief Get ATVV state
 * @return ATVV state.
 *******************************************************************************
 */
ble_atvvs_state_t rc_atvv_state(void);
#endif

/**
 *******************************************************************************
 * @brief Check if peer indicated codec is 8Khz.
 * @return True if codec is 8Khz. Otherwise is 16Khz.
 *******************************************************************************
 */
bool rc_atvv_is8k(void);

/**
 *******************************************************************************
 * @brief Check if current assistant model is legacy mode
 * @return True if using legacy mode
 *******************************************************************************
 */
bool rc_atvv_is_legacy_model(void);

/**
 *******************************************************************************
 * @brief Check if current assistant model is hold-to-talk mode(HTT)
 * @return True if using HTT mode
 *******************************************************************************
 */
bool rc_atvv_is_htt_model(void);

#ifdef CFG_ATVV_VER_100
/**
 *******************************************************************************
 * @brief Delete ATVV configuration
 *******************************************************************************
 */
void rc_atvv_del_config(void);
#endif

/**
 *******************************************************************************
 * @brief Initialize ATVV environment.
  *******************************************************************************
 */
void rc_atvv_init(void);

#ifdef CFG_PDM_LOCAL_TEST
/**
 *******************************************************************************
 * @brief ATVV test start
 * @note This is used for ATVV flow unit test locally.
 *******************************************************************************
 */
void rc_atvv_test_start(void);
/**
 *******************************************************************************
 * @brief Pretend the bad link situation without allocating buffers.
 * @note This is used for ATVV flow unit test locally.
 *******************************************************************************
 */
void rc_atvv_fake_bad_link(void);
#endif // CFG_PDM_LOCAL_TEST
