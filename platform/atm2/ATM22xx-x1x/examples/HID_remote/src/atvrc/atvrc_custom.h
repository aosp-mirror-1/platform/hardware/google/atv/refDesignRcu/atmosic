/**
 *******************************************************************************
 *
 * @file atvrc_custom.c
 *
 * @brief Android TV remote customization data interface
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#pragma once

#define ATVRC_PNP_ID_LEN 7
#define ATVRC_DEV_NAME_LEN_MAX 16
#define ATVRC_FW_REV_STR_LEN 5

#define ATVRC_LED_KEY_CS 10
#define ATVRC_LED_KEY_BLINK 1
#define ATVRC_LED_CFM_CS 10
#define ATVRC_LED_CFM_BLINK 2
#define ATVRC_LED_ERR_CS 5
#define ATVRC_LED_ERR_BLINK 4

/**
 * @brief Customization data initialization.
 */
void atvrc_custom_init(void);

/**
 * @brief Get device type
 *
 * @return Device type
 */
uint8_t atvrc_custom_get_device_type(void);

/**
 * @brief Get PNP ID
 *
 * @return PNP ID data pointer
 */
uint8_t *atvrc_custom_get_pnp(void);

/**
 * @brief Get device name
 *
 * @param[inout] len Input: device name length maximum, Output: actual device
 * name length
 * @param[out] name Device name data pointer
 * @return Status
 */
uint8_t atvrc_custom_get_dev_name(uint16_t *len, uint8_t *name);

/**
 * @brief Get UI layout bitmap
 *
 * @return UI layout bitmap
 */
uint8_t atvrc_custom_get_ui_layout(void);

/**
 * @brief Get firmware revision string
 *
 * @return Firmware revision string pointer
 */
uint8_t *atvrc_custom_get_fw_rev(void);

/**
 * @brief Check wake up key and set wake up configuration
 *
 * @param[in] bt_key BT keycode of pressed key
 *
 * @return Wake key ID or not a wake key
 */
uint8_t atvrc_custom_check_wake_key(uint32_t bt_key);

/**
 * @brief Is cutomized wake up
 *
 * @return True if customized wake up configuration exists, otherwise false
 */
bool atvrc_custom_is_cwake(void);

/**
 * @brief Is sending customized wake up packet only
 *
 * @return True for sending customized wake up packet only, otherwise false
 */
bool atvrc_custom_is_cwake_only(void);

/**
 * @brief Set customzied wake up packet payload
 *
 * @param[out] data Configured wake up packet payload
 * @param[in] key_id Wake key ID
 * @param[in] key_cnt Pressed key count
 * @param[in] addr Peer device address
 *
 * @return Length of customized wake up packet payload
 */
__NONNULL(1,4)
uint8_t atvrc_custom_set_cwake_pkt(uint8_t *data, uint8_t key_id,
    uint8_t key_cnt, uint8_t const *addr);

/**
 * @brief Is periodically wake up
 *
 * @return True if periodically wake up enabled, otherwise false
 */
bool atvrc_custom_is_period_wake(void);

/**
 * @brief Get periodically wake up interval
 *
 * @return Periodically wake up interval in minute
 */
uint16_t atvrc_custom_get_wake_interval(void);

/**
 * @brief Is remote RPA enabled
 *
 * @return True if remote RPA enabled, otherwise false
 */
bool atvrc_custom_is_rpa_enabled(void);

/**
 * @brief Is BLE advertisement enabled
 *
 * @return True if BLE advertisement enabled, otherwise false
 */
bool atvrc_custom_is_ble_adv_enabled(void);

/**
 * @brief Is cache power key
 *
 * @return True if cache power key enabled, otherwise false
 */
bool atvrc_custom_is_cache_pwr(void);