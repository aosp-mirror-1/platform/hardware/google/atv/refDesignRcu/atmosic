/**
 *******************************************************************************
 *
 * @file bridge_ir.h
 *
 * @brief Bridge IR service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

/**
 * @brief Get device type
 *
 * @return REMOTE_G10 or REMOTE_G20, see ATV remote control PRD for details
 */
uint8_t app_custom_get_device_type(void);

/**
 * @brief Send IR key codes
 *
 * @param[in] key_id IR key id
 * @param[in] code Pointer of IR code data
 * @param[in] size IR code data size
 */
void atm_ir_send_key_code(uint16_t key_id, uint8_t const *code, uint16_t size);

/**
 * @brief Disable slave latency
 *
 * @param[in] disable True for disabling slave latency. False for enabling slave
 * latency;
 */
void atm_disable_slave_latency(bool disable);

/**
 * @brief Write IR code
 *
 * @param[in] ir_idx IR key index
 * @param[in] data Pointer of IR code data
 * @param[in] size IR code data size
 */
void atm_ir_write_code(uint8_t ir_idx, uint8_t *data, uint16_t size);

/**
 * @brief Read IR code
 *
 * @param[in] ir_idx IR key index
 * @param[out] data Pointer of IR code data
 * @param[in] size IR code data size
 */
uint8_t atm_ir_read_code(uint8_t ir_idx, uint8_t *data, uint16_t size);

/**
 * @brief Write IR key envet notification
 *
 * @param[in] ccc Client characteristic configuration
 */
void atm_ir_write_ccc(uint8_t ccc);

/**
 * @brief Delete all IR code data
 */
void atm_ir_del_code(void);

/**
 * @brief Check ATV keycode and send universal IR key code
 *
 * @param[in] atv_keycode Android TV keycode
 *
 * @return True if IR code is sent, otherwise return false
 */
bool bridge_ir_check_key(uint16_t atv_keycode);

/**
 * @brief Clear all universal IR code data in flash
 */
void bridge_ir_clear(void);

/**
 * @brief Send universal IR key release
 */
uint8_t bridge_ir_key_release(void);

/**
 * @brief Bridge IR initialization
 */
void bridge_ir_init(void);