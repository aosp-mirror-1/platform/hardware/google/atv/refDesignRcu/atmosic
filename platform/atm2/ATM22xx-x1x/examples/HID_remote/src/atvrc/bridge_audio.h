/**
 *******************************************************************************
 *
 * @file bridge_audio.h
 *
 * @brief Bridge audio service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

#define VOICE_V0P4_ADPCM_PACKET_LEN 136
#define VOICE_V0P4_ADPCM_UNIT_SIZE 256
#define VOICE_V1P0_ADPCM_PACKET_LEN 128
#define VOICE_V1P0_ADPCM_UNIT_SIZE 240

typedef struct {
    uint8_t codec;
    uint16_t frame_no;
    uint16_t pred_val;
    uint8_t step_idx;
} __PACKED audio_sync_t;

/**
 * @brief Get audio sync information
 *
 * @return Audio sync information
 */
audio_sync_t *bridge_audio_get_sync_info(void);

/**
 * @brief Write voice data to audio buffer
 *
 * @param[in] data ADPCM data
 */
void bridge_audio_write_voice_data(uint8_t data);

/**
 * @brief Resend voice packet
 */
void bridge_audio_resend_voice(void);

/**
 * @brief Check legacy model
 *
 * @return True if ATVV is using legacy model, otherwise return false
 */
bool bridge_audio_is_legacy_model(void);

/**
 * @brief Check HTT(Hold-to-Talk) model
 *
 * @return True if ATVV is using HTT model, otherwise return false
 */
bool bridge_audio_is_htt_model(void);

/**
 * @brief Start voice search
 */
void bridge_audio_start_search(void);

/**
 * @brief D-Pad select to start search
 */
void bridge_audio_dpad_select(void);

/**
 * @brief Stop voice search
 */
void bridge_audio_stop(void);

/**
 * @brief Set MTU
 *
 * @param[in] mtu Peer device RX MTU size
 */
void bridge_audio_set_mtu(uint8_t mtu);

/**
 * @brief Set BLE packet size
 *
 * @param[in] pkt_size Peer packet size
 */
void bridge_audio_set_pkt_size(uint16_t pkt_size);

/**
 * @brief Bridge audio initialization
 */
void bridge_audio_init(void);

/**
 * @brief Voice service status indication
 *
 * @param[in] ready True if voice service is ready, otherwise false
 */
void bridge_audio_ready_ind(bool ready);

/**
 * @brief Read audio frame
 *
 * @return The pointer of audio frame data
 */
uint8_t *bridge_audio_read_frame(void);


/**
 * @brief Audio frame sent indication
 */
void bridge_audio_sent_frame_ind(void);

/// Porting clock time functions for gl_audio use
#include "timer.h"

/**
 * @brief Get the current clock time
 *
 * @return Current system based on the 32kHz clock
 */
inline static uint32_t clock_time(void)
{
    return atm_get_sys_time();
}

/**
 * @brief Check if clock time exceed
 *
 * @param[in] ref Reference clock time
 * @param[in] us Expected duration in microseconds
 *
 * @return True if the delta time is exceeded expected duration.
 */
inline static bool clock_time_exceed(uint32_t ref, uint32_t us)
{
    return (atm_lpc_to_us(clock_time() - ref) > us);
}

#define att_ccc_control bridge_audio_ccc_write
/**
 * @brief Audio service CCC write indication
 *
 * @param[in] msg Write CCC message
 *
 * @return Always return 0
 */
int bridge_audio_ccc_write(void* msg);