/**
 *******************************************************************************
 *
 * @file bridge_att.h
 *
 * @brief Bridge BLE ATT
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

/**
 * @brief Bridge BLE ATT initialization.
 */
void bridge_att_init(void);

void atm_att_setAttributeTable(uint8_t *atts);

uint8_t atm_att_pushNotifyData(uint16_t attHandle, uint8_t *data, int len);

#define bls_att_setAttributeTable atm_att_setAttributeTable
#define bls_att_pushNotifyData atm_att_pushNotifyData