/**
 *******************************************************************************
 *
 * @file bridge_att.c
 *
 * @brief Bridge BLE ATT
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#include <inttypes.h>
#include "atvrc_porting.h"
#include "ble_att.h"
#include "ble_atmprfs.h"
#include "bridge_audio.h"
#include "co_endian.h"
#include "vendor/827x_ble_remote/app_config.h"
#include "stack/ble/ble_format.h"
#include "stack/ble/host/attr/att.h"
#include "stack/ble/service/uuid.h"

#undef ATT_DEBUG

extern uint8_t device_in_connection_state;
static attribute_t *att_tbl;

void atm_array_printf(uint8_t const *data, uint16_t size)
{
#if PLF_DEBUG
    for (uint16_t i = 0; i < size; i++) {
	if (!(i % 16)) {
	    printf("\n");
	}
	printf("%02X ", *(data + i));
    }
    printf("\n");
#endif
}

#ifdef ATT_DEBUG
#define ATT_TRACE DEBUG_TRACE

static void print_uuid_128(uint8_t *uuid)
{
    printf("\t\tUUID: ");
    for (uint8_t i = 0; i < ATT_UUID_128_LEN; i++) {
	if (i && !(i % 4)) {
	    printf("-");
	}
	printf("%02X", uuid[i]);
    }
    printf("\n");
}
#else
#define ATT_TRACE(fmt, ...) DEBUG_TRACE_COND(0, fmt, ##__VA_ARGS__)
#endif

static uint8_t att_read_req_cb(uint8_t conidx, uint8_t att_idx)
{
    ATT_TRACE("%s: att_idx (%d)", __func__, att_idx);
    ble_atmprfs_gattc_read_cfm(conidx, att_idx, 0, 0);
    return ATT_ERR_NO_ERROR;
}

static rf_packet_att_data_t attd;
static uint8_t att_write_req_cb(uint8_t conidx, uint8_t att_idx,
    uint8_t const *data, uint16_t size)
{
    ATT_TRACE("%s: att_idx(%d) size(%d)", __func__, att_idx, size);
#ifdef ATT_DEBUG
    atm_array_printf(data, size);
#endif
#define ATT_HANDLE_OFFSET 1
#define ATT_HEADER_LEN 3
    attribute_t *att = att_tbl + att_idx + ATT_HANDLE_OFFSET;
    if (att->w) {
	attd.handle = att_idx + ATT_HANDLE_OFFSET;
	attd.l2cap = size + ATT_HEADER_LEN;
	memcpy(attd.dat, data, size);
	att->w(&attd);
    }
    return ATT_ERR_NO_ERROR;
}

static ble_atmprfs_cbs_t const cbs = {
    .read_req = att_read_req_cb,
    .write_req = att_write_req_cb,
};

void atm_att_setAttributeTable(uint8_t *atts)
{
    DEBUG_TRACE("%s", __func__);
#define ATT_TBL_NUM_ATTS 0
    uint8_t num_att = atts[ATT_TBL_NUM_ATTS];
    att_tbl = (attribute_t*)atts;
    for (uint8_t i = 0; i <= num_att; i++) {
	attribute_t *att = att_tbl + i;
	uint16_t uuid = 0;
	if (att->uuidLen == sizeof(uint16_t)) {
	    uuid = *((uint16_t*)att->uuid);
	}
	if (uuid == GATT_UUID_PRIMARY_SERVICE) {
	    ATT_TRACE("PRIMARY SERVICE (%04hX):", uuid);
	    uint8_t svc_uuid[ATT_UUID_128_LEN] = {0};
	    co_bswap(svc_uuid, att->pAttrValue, ATT_UUID_128_LEN);
#ifdef ATT_DEBUG
	    print_uuid_128(svc_uuid);
#endif
	    ble_atmprfs_add_svc(svc_uuid, BLE_SEC_PROP_NO_SECURITY, &cbs);
	} else if (uuid == GATT_UUID_CHARACTER) {
#define ATT_CHAR_PROP 0
#define ATT_CHAR_HANDLE 1
#define ATT_CHAR_UUID 3
	    uint8_t prop = att->pAttrValue[ATT_CHAR_PROP];
	    ATT_TRACE("\tCharacteristic (%04hX): %s%s%s%s%s", uuid,
		(prop & CHAR_PROP_READ) ? "READ |" : "",
		(prop & CHAR_PROP_WRITE_WITHOUT_RSP) ? " WRITE_WO_RSP |" : "",
		(prop & CHAR_PROP_WRITE) ? " WRITE |" : "",
		(prop & CHAR_PROP_NOTIFY) ? " NOTIFY |" : "",
		(prop & CHAR_PROP_INDICATE) ? " INDICATE" : "");
	    uint8_t char_uuid[ATT_UUID_128_LEN] = {0};
	    co_bswap(char_uuid, &(att->pAttrValue[ATT_CHAR_UUID]),
		ATT_UUID_128_LEN);
#ifdef ATT_DEBUG
	    printf("\t");
	    print_uuid_128(char_uuid);
#endif
	    ATT_TRACE("\t\tattribute len: %ld", att->attrLen);
	    uint16_t perm = (att->pAttrValue[ATT_CHAR_PROP] << 8) |
		((att->perm & ATT_PERMISSIONS_READ) ? PERM(RP, NO_AUTH) : 0 ) |
		((att->perm & ATT_PERMISSIONS_WRITE) ? PERM(WP, NO_AUTH) : 0 );
	    ble_atmprfs_add_char(char_uuid, perm, att->attrLen);
	} else if (uuid == GATT_UUID_CLIENT_CHAR_CFG) {
	    ATT_TRACE("\t\tClient Characteristic Configuration (%04hX)", uuid);
	    ble_atmprfs_add_client_char_cfg();
	}
    }
}

static void ntf_sent_cb(uint8_t conidx, ble_gattc_cmp_evt_ex_t const *parm,
    void const *ctx)
{
#ifdef CFG_ATVRC_AUDIO
    bridge_audio_resend_voice();
#endif
}

__FAST
uint8_t atm_att_pushNotifyData(uint16_t attHandle, uint8_t *data, int len)
{
#ifdef ATT_DEBUG
    if (attHandle != AUDIO_GOOGLE_RX_DP_H) {
	ATT_TRACE("%s attHandle: %d, len: %d", __func__, attHandle, len);
	atm_array_printf(data, len);
    }
#endif
    if (!device_in_connection_state) {
	ATT_TRACE("No connection");
	return 1;
    }
    uint8_t conidx = device_in_connection_state - 1;
    uint8_t status = ble_atmprfs_gattc_send_ntf(conidx,
	attHandle - ATT_HANDLE_OFFSET, data, len, ntf_sent_cb);
    if (status) {
	ATT_TRACE("%s handle: %d error: %02X", __func__, attHandle, status);
    }
    return status;
}

void bridge_att_init(void)
{
    extern void my_att_init(void);
    my_att_init();
}