/**
 *******************************************************************************
 *
 * @file bridge_ir.c
 *
 * @brief Bridge IR service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#include "arch.h"
#include "bridge_ir.h"
#include "nvds.h"
#include "rc_ir.h"
#include "rc_gap.h"
#ifdef CFG_ATVRC_CUSTOM
#include "atvrc_custom.h"
#else
#include "vendor/827x_ble_remote/app_config.h"
#endif

uint8_t app_custom_get_device_type(void)
{
#ifdef CFG_ATVRC_CUSTOM
    return atvrc_custom_get_device_type();
#else
    return REMOTE_G10;
#endif
}

void atm_disable_slave_latency(bool disable)
{
    rc_gap_local_slave_latency(disable);
}

void atm_ir_send_key_code(uint16_t key_id, uint8_t const *code, uint16_t size)
{
    DEBUG_TRACE("%s key_id: %d", __func__, key_id);
    rc_ir_send_uni_code(key_id, code, size);
}

#define NVDS_TAG_ATVRC_IR_CCC_CFG 0xC4
#define NVDS_TAG_ATVRC_IR_CODE_BASE 0xD0

void atm_ir_write_code(uint8_t ir_idx, uint8_t *data, uint16_t size)
{
    DEBUG_TRACE("%s index: %d, size: %d", __func__, ir_idx, size);
    nvds_tag_len_t len = size;
    nvds_put(NVDS_TAG_ATVRC_IR_CODE_BASE + ir_idx, len, data);
}

uint8_t atm_ir_read_code(uint8_t ir_idx, uint8_t *data, uint16_t size)
{
    DEBUG_TRACE("%s index: %d, size: %d", __func__, ir_idx, size);
    nvds_tag_len_t len = size;
    if (nvds_get(NVDS_TAG_ATVRC_IR_CODE_BASE + ir_idx, &len, data) == NVDS_OK) {
	return true;
    }
    return false;
}

void atm_ir_write_ccc(uint8_t ccc)
{
    DEBUG_TRACE("%s ccc: %d", __func__, ccc);
    nvds_tag_len_t len = sizeof(uint8_t);
    nvds_put(NVDS_TAG_ATVRC_IR_CCC_CFG, len, &ccc);
}

void atm_ir_del_code(void)
{
    DEBUG_TRACE("%s", __func__);
#define NUM_IR_CODE_TAG 5
    for (uint8_t i = 0; i < NUM_IR_CODE_TAG; i++) {
	nvds_del(NVDS_TAG_ATVRC_IR_CODE_BASE + i);
    }
}

extern uint8_t ir_key_is_suppress(uint8_t idx);
extern uint8_t ir_fallback_send_key_code(uint8_t button_idx, bool key_down);
extern void ir_table_init(void);
extern void ir_flash_factory(void);
extern int ir_fallback_send(uint8_t idx);
extern void ir_flash_check(uint32_t addr);

bool bridge_ir_check_key(uint16_t atv_keycode)
{
    if (!atv_keycode) {
	return false;
    }
    if (ir_key_is_suppress(atv_keycode)) {
	return false;
    }
    return ir_fallback_send_key_code(atv_keycode, 1);
}

void bridge_ir_clear(void)
{
    ir_table_init();
    ir_flash_factory();
}

uint8_t bridge_ir_key_release(void)
{
    return ir_fallback_send(0);
}

extern void ir_init_key_event_notify(uint8_t value);
void bridge_ir_init(void)
{
    uint8_t ccc;
    nvds_tag_len_t len = sizeof(uint8_t);
    if (nvds_get(NVDS_TAG_ATVRC_IR_CCC_CFG, &len, &ccc) == NVDS_OK) {
	DEBUG_TRACE("%s ccc: %d", __func__, ccc);
	ir_init_key_event_notify(ccc);
    } else {
	DEBUG_TRACE("%s: Fail to read ATVRC IR CCC NVDS TAG", __func__);
    }
    ir_flash_check(0);
}
