/**
 *******************************************************************************
 *
 * @file bridge_fms.h
 *
 * @brief Bridge Find Me Service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */
#pragma once

#include "arch.h"

#define ATM_FMS_BUF_SIZE 0x30

void bridge_fms_init(void);
void atm_fms_buzzer_play(uint8_t reason, unsigned char sound_level);
void atm_fms_buzzer_stop(void);
uint8_t atm_fms_buzzer_is_busy(void);

#define app_buzzer_play atm_fms_buzzer_play
#define app_buzzer_stop atm_fms_buzzer_stop
#define app_buzzer_is_buzy atm_fms_buzzer_is_busy