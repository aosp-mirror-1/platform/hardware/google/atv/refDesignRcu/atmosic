/**
 *******************************************************************************
 *
 * @file bridge_fms.c
 *
 * @brief Bridge Find Me Service
 *
 * Copyright (C) Atmosic 2022
 *
 *******************************************************************************
 */

#include <inttypes.h>
#include "atm_utils_c.h"
#include "atvrc_porting.h"
#include "atvrc_custom.h"
#include "bridge_fms.h"
#include "buzzer.h"
#include "atm_pm.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include "vendor/827x_ble_remote/app_find_me/app_fms.h"
#include "vendor/827x_ble_remote/app_find_me/app_buzzer.h"
#pragma GCC diagnostic pop

uint8_t atm_fms_buf[ATM_FMS_BUF_SIZE];

static pm_lock_id_t fms_lock_ret;

#define NOTE_BPM 150 // 400ms for quarter note
note_t note_seq[] = {
    {NOTE_C5, QUARTER_NOTE},
    {NOTE_D5, QUARTER_NOTE},
    {NOTE_E5, QUARTER_NOTE},
    {NOTE_F5, QUARTER_NOTE},
    {NOTE_G5, QUARTER_NOTE},
    {NOTE_A5, QUARTER_NOTE},
    {NOTE_B5, QUARTER_NOTE},
};

static void buzzer_stop_cb(void)
{
    atm_pm_unlock(fms_lock_ret);
}


void bridge_fms_init(void)
{
    app_fms_buffer_init();
#define FMS_CTRL_OFFSET 0x24
    app_fms_ctrl_t *ctrl = (app_fms_ctrl_t*)(atm_fms_buf + FMS_CTRL_OFFSET);
    ctrl->en_periodic_wakeup = atvrc_custom_is_period_wake();
    DEBUG_TRACE("%s: en_periodic_wakeup(%p) %d", __func__,
	&ctrl->en_periodic_wakeup, ctrl->en_periodic_wakeup);
    fms_lock_ret = atm_pm_alloc(PM_LOCK_RETENTION);
    buzzer_init(buzzer_stop_cb);
}

void atm_fms_buzzer_play(uint8_t reason, unsigned char vol_lv)
{
    atm_pm_lock(fms_lock_ret);
    buzzer_play(note_seq, ARRAY_LEN(note_seq), NOTE_BPM, vol_lv);
}

void atm_fms_buzzer_stop(void)
{
    buzzer_stop();
    atm_pm_unlock(fms_lock_ret);
}

uint8_t atm_fms_buzzer_is_busy(void)
{
    return buzzer_is_playing();
}