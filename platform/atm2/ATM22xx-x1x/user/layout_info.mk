
ifndef __LAYOUT_INFO_MK__
__LAYOUT_INFO_MK__ = 1

.PHONY: layout_info
layout_info:
	@echo "*────────────────────* $(FLASH_START)"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░UFLASH░░░░░░░░│($(if $(UFLASH_SIZE),$(UFLASH_SIZE),Default))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
ifdef USER_START
	@echo "├────────────────────┤ $(USER_START)"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░USER░░░░░░░░│($(USER_SIZE))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
endif
	@echo "├────────────────────┤ $(NVDS_START)"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░NVDS░░░░░░░░│($(if $(NVDS_SIZE),$(NVDS_SIZE),Default))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
ifneq (,$(filter OTAPS,$(PROFILES)))
ifdef MPR_START
	@echo "├────────────────────┤ $(shell printf "0x%x" $$(($(NVDS_START) + $(NVDS_SIZE))))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░reserved░░░░░░│($(MPR_SIZE))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░Bank 0░░░░░░░░│"
	@echo "├====================┤ $(FLASH_SIZE)"
	@echo ".░░░░░░Bank 1░░░░░░░░."
	@echo ".░░░░░░░░░░░░░░░░░░░░."
	@echo ".░░░░░░░░░░░░░░░░░░░░."
	@echo ".░░░░░░░░░░░░░░░░░░░░."
	@echo "├────────────────────┤ $(MPR_START)"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░MPR░░░░░░░░░│($(MPR_SIZE))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "*────────────────────* $(shell printf "0x%x" $$(($(FLASH_SIZE)*2)))"
else
	@echo "*────────────────────* $(FLASH_SIZE)"
endif # MPR_START
else
ifdef MPR_START
	@echo "├────────────────────┤ $(MPR_START)"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
	@echo "│░░░░░░░░MPR░░░░░░░░░│($(MPR_SIZE))"
	@echo "│░░░░░░░░░░░░░░░░░░░░│"
endif # MPR_START
	@echo "*────────────────────* $(FLASH_SIZE)"
endif # OTAPS

endif # __LAYOUT_INFO_MK__
