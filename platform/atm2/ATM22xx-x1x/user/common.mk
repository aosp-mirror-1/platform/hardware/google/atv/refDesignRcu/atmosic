
ifndef __COMMON_MK__
__COMMON_MK__ = 1

# First target
.PHONY: all
all:

.PHONY: FORCE
FORCE:

PLATFORM_DIR := $(realpath $(dir $(patsubst %/,%,$(dir $(lastword $(filter-out $(lastword $(MAKEFILE_LIST)), $(MAKEFILE_LIST))))))/../..)
TOP_DIR := $(realpath $(PLATFORM_DIR)/../../../)
COMMON_USER_DIR := $(PLATFORM_DIR)/user
USER_DIR := $(PLATFORM_DIR)/user
BLE_APP_DIR := $(PLATFORM_DIR)/app
DRIVER_DIR := $(PLATFORM_DIR)/driver
GDB_CFG_DIR := $(PLATFORM_DIR)/gdb
INCLUDE_DIR := $(PLATFORM_DIR)/include
OPENOCD_CFG_DIR := $(PLATFORM_DIR)/openocd
BLE_PRF_DIR := $(PLATFORM_DIR)/profiles
ROM_DIR := $(PLATFORM_DIR)/rom
PLAT_TOOLS_DIR := $(PLATFORM_DIR)/tools
LIB_DIR := $(PLATFORM_DIR)/lib
EXAMPLE_DIR := $(PLATFORM_DIR)/examples

to_upper = $(shell echo $1 | tr '[:lower:]' '[:upper:]')

wdog_val = $(shell printf "0x%08x" `expr $1 \* 16000000`)

wdog_tag = $(shell printf "%02x " "$$(( $(call wdog_val,$1) & 0xFF ))" ; \
	    printf "%02x " "$$(( ($(call wdog_val,$1) & 0xFF00)  >> 8 ))" ; \
	    printf "%02x " "$$(( ($(call wdog_val,$1) & 0xFF0000) >> 16))" ; \
	    printf "%02x" "$$(( ($(call wdog_val,$1) & 0xFF000000) >> 24))" )

PLATFORM_NAME := $(notdir $(PLATFORM_DIR))
PLATFORM_FAMILY := $(notdir $(realpath $(PLATFORM_DIR)/..))

BOARD ?= m2221
ifneq (,$(filter-out m2201 m2202 m2221 m2231 m2251 m3201 m3202 m3221 m3231 x2xx_emu x2xx_mp,$(BOARD)))
    $(error "usage: make $(MAKECMDGOALS) BOARD=<m2201|m2202|m2221|m2231|m2251|m3201|m3202|m3221|m3231")
endif

endif # __COMMON_MK__
