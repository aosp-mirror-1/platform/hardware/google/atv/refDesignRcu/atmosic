################################################################################
#
# @file nvds.mk
#
# @brief NVDS make helper
#
# Copyright (C) Atmosic 2018-2022
#
################################################################################

ifndef __NVDS_MK__
__NVDS_MK__ = 1

include $(COMMON_USER_DIR)/tools.mk
include $(ROM_DIR)/rom.mk

DEF_MPR_BIN_FILE := flash_mpr_nvds.bin

ifdef MPR_SIZE
ifneq ($(shell printf "%d" $$(($(MPR_SIZE)&0x0FFF))),0)
$(error "MPR_SIZE is not at 4K boundary")
endif

ifndef FLASH_SIZE
$(error "usage: Need to compile with FLASH_SIZE. ex: make FLASH_SIZE=0x40000 MPR_SIZE=0x1000")
endif #FLASH_SIZE
endif

ifneq ($(or $(MPR_SIZE),$(USER_SIZE),$(FLASH_SIZE),$(NVDS_SIZE)),)
FLASH_SIZE ?= 0x80000
NVDS_SIZE ?= 0x8000
MPR_SIZE ?= 0
USER_SIZE ?= 0
FLASH_MEM_BANK ?= "bank0"
FLASH_START := 0

ifeq ($(MPR_SIZE),0)
NVDS_START := $(shell printf "0x%x" $$(($(FLASH_SIZE) - $(NVDS_SIZE))))
else
ifneq (,$(filter OTAPS,$(PROFILES)))
MPR_START := $(shell printf "0x%x" $$(($(FLASH_SIZE)*2 - $(MPR_SIZE))))
NVDS_START := $(shell printf "0x%x" $$(($(FLASH_SIZE) - $(MPR_SIZE) - $(NVDS_SIZE))))
else
MPR_START := $(shell printf "0x%x" $$(($(FLASH_SIZE) - $(MPR_SIZE))))
NVDS_START := $(shell printf "0x%x" $$(($(MPR_START) - $(NVDS_SIZE))))
endif #OTAPS
endif #MPR_SIZE

ifneq ($(USER_SIZE),0)
USER_START := $(shell printf "0x%x" $$(($(NVDS_START) - $(USER_SIZE))))
endif #USER_SIZE

UFLASH_SIZE := $(shell printf "0x%x" $$(($(FLASH_SIZE) - $(NVDS_SIZE) - $(USER_SIZE) - $(MPR_SIZE))))
endif

TAG_DATA_DIR ?= tag_data

.PHONY: all
all:

$(TAG_DATA_DIR)/%.bin: $(TAG_DATA_DIR)/%.tds
	sed 's,#.*$$,,' < $< | tr -d '[:space:]' | xxd -ps -r > $@

pre_bond_files := $(basename $(foreach file,$(TAG_DATA_DIR)/*,$(wildcard $(file)/*_pre_bond.tds)))
ifdef pre_bond_files
flash_nvds.data += $(foreach file,$(pre_bond_files),$(subst $(TAG_DATA_DIR)/,,$(file)))
endif

ifdef WDOG
flash_nvds.data += b5-SYDNEY_TAG_WATCHDOG/wdog
otp_nvds.data += b5-SYDNEY_TAG_WATCHDOG/wdog
endif

$(TAG_DATA_DIR)/18-PROG_DELAY:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/18-PROG_DELAY/%.tds
$(TAG_DATA_DIR)/18-PROG_DELAY/%.tds: | $(TAG_DATA_DIR)/18-PROG_DELAY
	echo "# Programing delay (margin for programing the baseband in advance of each activity in half-slots)" > $@
	echo $(18-PROG_DELAY.$(@F)) >> $@

ifeq ($(filter 18-PROG_DELAY/%,$(flash_nvds.data)),)
18-PROG_DELAY.default.tds := "04" # 4 half slots (1250us)
flash_nvds.data += 18-PROG_DELAY/default
endif

clean::
	rm -f $(TAG_DATA_DIR)/18-PROG_DELAY/default.tds

$(TAG_DATA_DIR)/2e-SLEEP_ALGO_DUR:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/2e-SLEEP_ALGO_DUR/%.tds
$(TAG_DATA_DIR)/2e-SLEEP_ALGO_DUR/%.tds: | $(TAG_DATA_DIR)/2e-SLEEP_ALGO_DUR
	echo $(2e-SLEEP_ALGO_DUR.$(@F)) > $@

ifeq ($(filter 2e-SLEEP_ALGO_DUR/%,$(flash_nvds.data)),)
2e-SLEEP_ALGO_DUR.sleep_algo.tds := "F4 01" # 500 half-us (250 us)
flash_nvds.data += 2e-SLEEP_ALGO_DUR/sleep_algo
otp_nvds.data += 2e-SLEEP_ALGO_DUR/sleep_algo
endif

clean::
	rm -f $(TAG_DATA_DIR)/2e-SLEEP_ALGO_DUR/sleep_algo.tds

ifdef LPC_RCOS
$(TAG_DATA_DIR)/0d-EXT_WAKEUP_TIME:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/0d-EXT_WAKEUP_TIME/%.tds
$(TAG_DATA_DIR)/0d-EXT_WAKEUP_TIME/%.tds: | $(TAG_DATA_DIR)/0d-EXT_WAKEUP_TIME
	echo "# 9ms in us" > $@
	echo $(0d-EXT_WAKEUP_TIME.$(@F)) > $@

$(TAG_DATA_DIR)/0e-OSC_WAKEUP_TIME:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/0e-OSC_WAKEUP_TIME/%.tds
$(TAG_DATA_DIR)/0e-OSC_WAKEUP_TIME/%.tds: | $(TAG_DATA_DIR)/0e-OSC_WAKEUP_TIME
	echo "# 9ms in us" > $@
	echo $(0e-OSC_WAKEUP_TIME.$(@F)) > $@

$(TAG_DATA_DIR)/2b-SLEEP_ADJ:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/2b-SLEEP_ADJ/%.tds
$(TAG_DATA_DIR)/2b-SLEEP_ADJ/%.tds: | $(TAG_DATA_DIR)/2b-SLEEP_ADJ
	echo "# Number of half slots (312.5us)" > $@
	echo $(2b-SLEEP_ADJ.$(@F)) > $@

0d-EXT_WAKEUP_TIME.no_32KHz_xtal.tds := "28 23"
0e-OSC_WAKEUP_TIME.no_32KHz_xtal.tds := "28 23"
2b-SLEEP_ADJ.no_32KHz_xtal.tds := "02 00 00 00"

flash_nvds.data += 0d-EXT_WAKEUP_TIME/no_32KHz_xtal \
    0e-OSC_WAKEUP_TIME/no_32KHz_xtal \
    2b-SLEEP_ADJ/no_32KHz_xtal \

clean::
	rm -f $(TAG_DATA_DIR)/0d-EXT_WAKEUP_TIME/no_32KHz_xtal.tds
	rm -f $(TAG_DATA_DIR)/0e-OSC_WAKEUP_TIME/no_32KHz_xtal.tds
	rm -f $(TAG_DATA_DIR)/2b-SLEEP_ADJ/no_32KHz_xtal.tds
endif # LPC_RCOS

ifdef USER_BD_ADDR
flash_nvds.data += 01-BD_ADDRESS/user

$(TAG_DATA_DIR)/01-BD_ADDRESS:
	mkdir -p $@

.PHONY: $(TAG_DATA_DIR)/01-BD_ADDRESS/user.tds
$(TAG_DATA_DIR)/01-BD_ADDRESS/user.tds: | $(TAG_DATA_DIR)/01-BD_ADDRESS
	echo $(USER_BD_ADDR) > $@
endif

$(TAG_DATA_DIR)/b3-FLASH_POWERUP_DELAY:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/b3-FLASH_POWERUP_DELAY/%.tds
$(TAG_DATA_DIR)/b3-FLASH_POWERUP_DELAY/%.tds: | $(TAG_DATA_DIR)/b3-FLASH_POWERUP_DELAY
	echo $(b3-FLASH_POWERUP_DELAY.$(@F)) > $@

ifeq ($(filter b3-FLASH_POWERUP_DELAY/%,$(otp_nvds.data)),)
# Use Macronix delay as the superset default value
b3-FLASH_POWERUP_DELAY.macronix.tds := "1a 00" # 800us @ 32.768khz
otp_nvds.data += b3-FLASH_POWERUP_DELAY/macronix
endif

clean::
	rm -f $(TAG_DATA_DIR)/b3-FLASH_POWERUP_DELAY/macronix.tds

ifdef NVDS_APP
otp_nvds.data += b6-USER_APP/$(NVDS_APP)

all: $(TAG_DATA_DIR)/b6-USER_APP/$(NVDS_APP).bin

.PHONY: gdb_otp
gdb_otp: $(TAG_DATA_DIR)/b6-USER_APP/$(NVDS_APP).elf $(TAG_DATA_DIR)/b6-USER_APP/rom.elf $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/sydney.gdb $< -ex "add-symbol-file $(TAG_DATA_DIR)/b6-USER_APP/rom.elf"

.PHONY: gdb_otp_target
gdb_otp_target: $(TAG_DATA_DIR)/b6-USER_APP/$(NVDS_APP).elf $(TAG_DATA_DIR)/b6-USER_APP/rom.elf $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/sydney.gdb $< -ex "add-symbol-file $(TAG_DATA_DIR)/b6-USER_APP/rom.elf" -ex "target remote $(GDB_REMOTE)"
endif

NVDS_APP_INCLUDES := \
	$(INCLUDE_DIR) \
	$(INCLUDE_DIR)/armgcc \
	$(INCLUDE_DIR)/reg \
	$(INCLUDE_DIR)/reg_ble \
	$(INCLUDE_DIR)/arm \
	$(INCLUDE_DIR)/ble \
	$(DRIVER_DIR)/flash \
	$(DRIVER_DIR)/spi \
	$(DRIVER_DIR)/timer \
	$(LIB_DIR)/atm_utils_c \
	$(LIB_DIR)/atm_utils_math \
	$(NVDS_APP_INCLUDES)

$(TAG_DATA_DIR)/b6-USER_APP/rom.elf: $(ROM_DIR)/fw.elf
	$(OBJCOPY) $(NVDS_APP_ROMCOPY_FLAGS) $< $@

$(TAG_DATA_DIR)/b6-USER_APP/rom.o: $(TAG_DATA_DIR)/b6-USER_APP/rom.elf
	$(COMMON_USER_DIR)/gen_symtab.py $< $@

.PRECIOUS: $(TAG_DATA_DIR)/b6-USER_APP/%.elf
$(TAG_DATA_DIR)/b6-USER_APP/%.elf: $(TAG_DATA_DIR)/b6-USER_APP/%.c \
    $(TAG_DATA_DIR)/b6-USER_APP/rom.o
	cd $(@D) && $(GCC) -mcpu=cortex-m0 -Os -mthumb -mlong-calls \
	    -fms-extensions -ffunction-sections -fdata-sections \
	    -fno-strict-aliasing -g3 -flto \
	    -Wall -Wchar-subscripts -Wformat -Wformat-signedness \
	    -Wuninitialized -Winit-self \
	    -Wignored-qualifiers -Wswitch-default -Wswitch-enum -Wunused -Wundef \
	    -Wnull-dereference -Wsuggest-attribute=noreturn -Wdouble-promotion \
	    -Wwrite-strings -Wshadow -Wcast-qual \
	    -Wpointer-arith \
	    -Wstrict-prototypes -Wold-style-declaration -Wold-style-definition \
	    -Wmissing-parameter-type \
	    -std=c17 -Werror \
	    -DCORTEX_M0 $(ROM_CFLAGS) -DCFG_USER \
	    $(NVDS_APP_CFLAGS) \
	    $(NVDS_APP_INCLUDES:%=-I%) \
	    -MD $(<F) \
	    -Wl,--fatal-warnings,--warn-common \
	    -nostdlib -Wl,--gc-sections \
	    -L$(ROM_DIR) -L$(USER_DIR) -Tcmsdk_cm0_user_ram.ld \
	    $(if $(NVDS_APP_URAM_SIZE),-Wl$(comma)--defsym=URAM_SIZE=$(NVDS_APP_URAM_SIZE)) \
	    $(word 2,$(^F)) \
	    -o $(@F)
-include $(TAG_DATA_DIR)/b6-USER_APP/*.d

$(TAG_DATA_DIR)/b6-USER_APP/%.bin: $(TAG_DATA_DIR)/b6-USER_APP/%.elf
	$(OBJCOPY) -O binary $< $@

$(TAG_DATA_DIR)/b5-SYDNEY_TAG_WATCHDOG:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/b5-SYDNEY_TAG_WATCHDOG/%.tds
$(TAG_DATA_DIR)/b5-SYDNEY_TAG_WATCHDOG/%.tds: | $(TAG_DATA_DIR)/b5-SYDNEY_TAG_WATCHDOG
	echo $(call wdog_tag,$(WDOG)) > $@

clean::
	rm -f $(TAG_DATA_DIR)/b5-SYDNEY_TAG_WATCHDOG/wdog.tds

$(TAG_DATA_DIR)/fe-MEM_RMW:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/fe-MEM_RMW/%.tds
$(TAG_DATA_DIR)/fe-MEM_RMW/%.tds: | $(TAG_DATA_DIR)/fe-MEM_RMW
	echo $($(@F)) > $@

$(TAG_DATA_DIR)/fc-PMU_W:
	mkdir -p $@

.PRECIOUS: $(TAG_DATA_DIR)/fc-PMU_W/%.tds
$(TAG_DATA_DIR)/fc-PMU_W/%.tds: | $(TAG_DATA_DIR)/fc-PMU_W
	echo $($(@F)) > $@

data2deps = $(addprefix $(TAG_DATA_DIR)/,$(addsuffix .bin,$1))
data2opts = $(foreach data,$1,-t $(firstword $(subst -, ,$(data))) -d $(TAG_DATA_DIR)/$(data).bin)

HARV_CFG := $(filter -DCFG_NONRF_HARV -DCFG_RF_HARV,$(CFLAGS))

RECHARGE_BATT := $(filter -DCFG_RECHBATT,$(CFLAGS))
ifneq ($(RECHARGE_BATT),)

PMU13.tds := "093800000100"
otp_nvds.data += fc-PMU_W/PMU13

ifeq ($(PMU_CFG),VBAT_GT_1p8V_VDDIO_EXT)
otp_nvds.hw := OTP_BATT_TYPE:RECHARGE_VBAT_GT_1p8V OTP_DISABLE_VDDIO
else
otp_nvds.hw := OTP_BATT_TYPE:RECHARGE_VBAT_GT_1p8V
endif # VBAT_GT_1p8V_VDDIO_EXT

SWREG_CTRL_5.tds := "0c141a0a4e01"
SWREG_CTRL_6.tds := "0c184d0a8c09"
otp_nvds.data += fc-PMU_W/SWREG_CTRL_5
otp_nvds.data += fc-PMU_W/SWREG_CTRL_6

else # RECHARGE_BATT

ifneq ($(PMU_CFG),)
ifeq ($(PMU_CFG),VBAT_GT_1p8V_VDDIO_EXT)
otp_nvds.hw := OTP_BATT_TYPE:VBAT_GT_1p8V OTP_DISABLE_VDDIO
else ifeq ($(PMU_CFG),VBAT_GT_1p8V_VDDIO_INT)
otp_nvds.hw := OTP_BATT_TYPE:VBAT_GT_1p8V
else ifeq ($(PMU_CFG),VBAT_LE_1p8V)
otp_nvds.hw := OTP_BATT_TYPE:VBAT_LE_1p8V
else ifeq ($(PMU_CFG),NO_VBAT)
ifeq ($(HARV_CFG),)
$(error No harvesting option specified when using NO_VBAT)
endif
otp_nvds.hw := OTP_BATT_TYPE:NO_VBAT
else
$(warning Unknown PMU_CFG $(PMU_CFG))
endif
endif # PMU_CFG

endif # RECHARGE_BATT

ifneq ($(HARV_CFG),)
ifeq ($(HARV_CFG),-DCFG_NONRF_HARV)

NONRF_HARV.tds := "09406E5a0000"
otp_nvds.data += fc-PMU_W/NONRF_HARV

ifneq ($(filter -DVSTORE_MAX_EQ_3p0V,$(CFLAGS)),)
NONRF_VRANGE_1p1_1p4.tds := "0918211c1400"
NONRF_VRANGE_1p2_1p5.tds := "0918215c1600"
NONRF_VRANGE_1p3_1p6.tds := "0918219c1800"
NONRF_VRANGE_1p4_1p7.tds := "091821dc1a00"
NONRF_VRANGE_1p5_1p8.tds := "0918211c1d00"
NONRF_VRANGE_1p6_1p9.tds := "0918215c1f00"
else
NONRF_VRANGE_1p1_1p4.tds := "0918211e1400"
NONRF_VRANGE_1p2_1p5.tds := "0918215e1600"
NONRF_VRANGE_1p3_1p6.tds := "0918219e1800"
NONRF_VRANGE_1p4_1p7.tds := "091821de1a00"
NONRF_VRANGE_1p5_1p8.tds := "0918211e1d00"
NONRF_VRANGE_1p6_1p9.tds := "0918215e1f00"
endif # VSTORE_MAX_EQ_3p0V

ifneq ($(filter -DCFG_NONRF_VRANGE_1p1_1p4,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p1_1p4
else ifneq ($(filter -DCFG_NONRF_VRANGE_1p2_1p5,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p2_1p5
else ifneq ($(filter -DCFG_NONRF_VRANGE_1p3_1p6,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p3_1p6
else ifneq ($(filter -DCFG_NONRF_VRANGE_1p4_1p7,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p4_1p7
else ifneq ($(filter -DCFG_NONRF_VRANGE_1p5_1p8,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p5_1p8
else ifneq ($(filter -DCFG_NONRF_VRANGE_1p6_1p9,$(CFLAGS)),)
otp_nvds.data += fc-PMU_W/NONRF_VRANGE_1p6_1p9
else
$(error Voltage Range Config not defined in $(CFLAGS))
endif # CFG_NONRF_VRANGE_XX_XX

else # CFG_NONRF_HARV

otp_nvds.hw += OTP_HARV_TYPE
MDM_AGCCNTL_CLR_PEAKDET.tds := "306000500000000001000000"
otp_nvds.data += fe-MEM_RMW/MDM_AGCCNTL_CLR_PEAKDET

ifneq ($(filter -DVSTORE_MAX_EQ_3p0V,$(CFLAGS)),)
PMU5.tds := "0918215c1f00"
otp_nvds.data += fc-PMU_W/PMU5
endif # VSTORE_MAX_EQ_3p0V

endif # CFG_RF_HARV
endif # HARV_CFG

BRWNOUT_OPT := $(filter -DBRWNOUT_THR%,$(CFLAGS))
ifdef BRWNOUT_OPT
otp_nvds.hw += OTP_BROWNOUT_THR:$(BRWNOUT_OPT:-DBRWNOUT_THR%=%)
endif

ifneq ($(or $(otp_nvds.data),$(otp_nvds.hw)),)

nvds_tool_otp_args = $(call data2opts,$(otp_nvds.data)) $(if $(otp_nvds.hw),$(addprefix -w ,$(otp_nvds.hw)))

.PHONY: build_archive_otp_nvds
build_archive_otp_nvds: $(NVDS_TOOL) $(call data2deps,$(otp_nvds.data))
	$< -i $(nvds_tool_otp_args) -b -o otp_nvds.nvm

.PHONY: rebuild_otp_nvds
rebuild_otp_nvds: $(NVDS_TOOL) $(call data2deps,$(otp_nvds.data))
	$< -i -r otp_nvds.nvm $(nvds_tool_otp_args) -b -o otp_nvds_new.nvm
	mv -f otp_nvds.nvm otp_nvds_$(shell $(MD5SUM) otp_nvds.nvm | cut -d " " -f 1).nvm
	mv otp_nvds_new.nvm otp_nvds.nvm

otp_nvds.nvm:
	@echo "ERROR: Use build_archive_otp_nvds/rebuild_otp_nvds to make otp_nvds.nvm before invoking build_archive"
	@exit 1

.PHONY: arch_add_otp_nvds
arch_add_otp_nvds: otp_nvds.nvm
	$(call ATM_ISP_LOAD,$<,OtpNvds)

ifdef ADD_OTP_TO_ARCHIVE
build_archive: arch_add_otp_nvds
endif
endif # otp_nvds.data

NVDS_TOOL_F = $(if $(NVDS_SIZE),-f $(shell printf '%u' $(NVDS_SIZE)))
NVDS_TOOL_MPR_F = $(if $(MPR_SIZE),-f $(shell printf '%u' $(MPR_SIZE)))

ifdef flash_nvds.data
.PHONY: build_flash_nvds
build_flash_nvds: $(NVDS_TOOL) $(call data2deps,$(flash_nvds.data))
	$< $(NVDS_TOOL_F) -b $(call data2opts,$(flash_nvds.data)) > flash_nvds.bin

.PHONY: rebuild_flash_nvds
rebuild_flash_nvds: $(NVDS_TOOL) $(call data2deps,$(flash_nvds.data))
	$< $(NVDS_TOOL_F) -b -r flash_nvds.bin $(call data2opts,$(flash_nvds.data)) > flash_nvds_new.bin
	mv -f flash_nvds.bin flash_nvds_$(shell $(MD5SUM) flash_nvds.bin | cut -d " " -f 1).bin
	mv flash_nvds_new.bin flash_nvds.bin

flash_nvds.bin:
	@echo "ERROR: Use re/build_flash_nvds to make flash_nvds.bin before invoking build_archive"
	@exit 1

.PHONY: arch_add_flash_nvds
arch_add_flash_nvds: flash_nvds.bin
	$(call ATM_ISP_LOAD,$< $(if $(NVDS_START),$(NVDS_START) $(if $(NVDS_SIZE),$(NVDS_SIZE))),FlashNvds)

build_archive: arch_add_flash_nvds
endif # flash_nvds.data

.PHONY: pull_otp_nvds
pull_otp_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_dump_nvm otp_nvds.nvm; exit"

.PHONY: show_otp_nvds
show_otp_nvds:
	$(NVDS_TOOL) -i -r otp_nvds.nvm -v

.PHONY: show_pretty_otp_nvds
show_pretty_otp_nvds: $(NVDS_TOOL)
	$(NVDS_TOOL) -i -r otp_nvds.nvm -p


.PHONY: show_otp_hw_cfg
show_otp_hw_cfg:
	$(NVDS_TOOL) -i -r otp_nvds.nvm -v -N

.PHONY: show_pretty_otp_hw_cfg
show_pretty_otp_hw_cfg:
	$(NVDS_TOOL) -i -r otp_nvds.nvm -p -N

.PHONY: split_otp_nvds
split_otp_nvds:
	$(NVDS_TOOL) -i -r otp_nvds.nvm -S

.PHONY: push_otp_nvds
push_otp_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_burn_nvm otp_nvds.nvm; sydney_verify_nvm otp_nvds.nvm; exit"

.PHONY: pull_flash_nvds
pull_flash_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_dump_nvds flash_nvds.bin $(if $(NVDS_START),$(NVDS_START) $(if $(NVDS_SIZE),$(NVDS_SIZE) $(if $(FLASH_MEM_BANK),$(FLASH_MEM_BANK)))); exit"

.PHONY: show_flash_nvds
show_flash_nvds:
	$(NVDS_TOOL) $(NVDS_TOOL_F) -r flash_nvds.bin -v

.PHONY: show_pretty_flash_nvds
show_pretty_flash_nvds:
	$(NVDS_TOOL) $(NVDS_TOOL_F) -r flash_nvds.bin -p

.PHONY: split_flash_nvds
split_flash_nvds:
	$(NVDS_TOOL) $(NVDS_TOOL_F) -r flash_nvds.bin -S

.PHONY: erase_flash_nvds
erase_flash_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_erase_nvds $(if $(NVDS_START),$(NVDS_START) $(if $(NVDS_SIZE),$(NVDS_SIZE))); exit"

.PHONY: push_flash_nvds
push_flash_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_load_nvds flash_nvds.bin $(if $(NVDS_START),$(NVDS_START) $(if $(NVDS_SIZE),$(NVDS_SIZE))); exit"

ifdef MPR_START
.PHONY: pull_mpr_nvds
pull_mpr_nvds: reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_dump_nvds $(DEF_MPR_BIN_FILE) $(if $(MPR_START),$(MPR_START) $(if $(MPR_SIZE),$(MPR_SIZE))); exit"

.PHONY: show_mpr_nvds
show_mpr_nvds:
	$(NVDS_TOOL) $(NVDS_TOOL_MPR_F) -r $(DEF_MPR_BIN_FILE) -v

.PHONY: show_pretty_mpr_nvds
show_pretty_mpr_nvds:
	$(NVDS_TOOL) $(NVDS_TOOL_MPR_F) -r $(DEF_MPR_BIN_FILE) -p
endif # MPR_START

ifdef USER_START
.PHONY: erase_user
erase_user: check_openocd reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_erase_nvds $(USER_START) $(USER_SIZE); exit"
endif

ifndef APP
.PHONY: iar_gen keil_arm_gen keil_gcc_gen ses_gen
iar_gen: ide_not_support
keil_arm_gen: ide_not_support
keil_gcc_gen: ide_not_support
ses_gen: ide_not_support
ide_not_support:
	@echo "NVDS only application doesn't support IDE project generation"
endif

.PHONY: clean
clean::
	-rm -f $(TAG_DATA_DIR)/*/*.bin
	-rm -f $(TAG_DATA_DIR)/*/*_pre_bond.*
	-rm -f $(TAG_DATA_DIR)/b6-USER_APP/*.o
	-rm -f $(TAG_DATA_DIR)/b6-USER_APP/*.d
	-rm -f $(TAG_DATA_DIR)/b6-USER_APP/*.elf

endif # __NVDS_MK__
