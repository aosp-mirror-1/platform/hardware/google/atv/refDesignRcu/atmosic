
ifndef __TOOLS_MK__
__TOOLS_MK__ = 1

ifeq ($(OS),Windows_NT)
MD5SUM := md5sum
SED := sed
else
ifeq ($(shell uname),Darwin)
OS := Darwin
MD5SUM := md5 -r
SED := gsed
else
OS := Linux
MD5SUM := md5sum
SED := sed
endif
endif

TOOLS_DIR := $(realpath $(PLATFORM_DIR)/../../../tools)

ifeq ($(ATM_SDK_TOOLS_IN_PATH),)
BINUTILS_DIR := $(TOOLS_DIR)/gcc-arm-none-eabi-10.3-2021.07/bin
ifeq ($(BINUTILS_DIR),)
$(error BINUTILS_DIR unset)
endif
endif

BINUTILS_PREFIX_BASE := arm-none-eabi-
ifeq ($(ATM_SDK_TOOLS_IN_PATH),)
BINUTILS_PREFIX := $(BINUTILS_DIR)/$(BINUTILS_PREFIX_BASE)
else
BINUTILS_PREFIX := $(BINUTILS_PREFIX_BASE)
WHICH_GCC := $(shell which $(BINUTILS_PREFIX_BASE)gcc)
ifeq ($(OS),Windows_NT)
BINUTILS_DIR_TXT := directory of $(WHICH_GCC)
else
BINUTILS_DIR_TXT := $(dir $(WHICH_GCC))
endif
endif

ifeq ($(TOOLSET),IAR)
IAR_DIR := $(TOOLS_DIR)/bxarm-9.10.2/arm/bin
ASSEMBLE := $(IAR_DIR)/iasmarm
COMPILE := $(IAR_DIR)/iccarm
CXX := $(IAR_DIR)/iccarm --c++
LINK := $(IAR_DIR)/ilinkarm
else ifeq ($(TOOLSET),ARM)
ARM_DIR := $(TOOLS_DIR)/ARMCompiler6.16/bin
ASSEMBLE := $(ARM_DIR)/armasm
COMPILE := $(ARM_DIR)/armclang --target=arm-arm-none-eabi
CXX := $(ARM_DIR)/armclang --target=arm-arm-none-eabi -x c++
LINK := $(ARM_DIR)/armlink
else
ASSEMBLE := $(BINUTILS_PREFIX)as
COMPILE := $(BINUTILS_PREFIX)gcc
CXX := $(BINUTILS_PREFIX)c++
GCOV := $(BINUTILS_PREFIX)gcov
LINK := $(BINUTILS_PREFIX)gcc
PLUGIN := $(shell $(COMPILE) --print-file-name=liblto_plugin$(if $(filter Windows_NT,$(OS)),-0.dll,.so))
endif

GCC := $(BINUTILS_PREFIX)gcc
GDB := $(BINUTILS_PREFIX)gdb
NM := $(BINUTILS_PREFIX)nm
OBJCOPY := $(BINUTILS_PREFIX)objcopy
OBJDUMP := $(BINUTILS_PREFIX)objdump
SIZE := $(BINUTILS_PREFIX)size
AR := $(BINUTILS_PREFIX)ar
RANLIB := $(BINUTILS_PREFIX)ranlib

$(sort $(foreach t,ASSEMBLE COMPILE CXX GCOV LINK GCC GDB NM OBJCOPY OBJDUMP \
    SIZE AR RANLIB,$(firstword $($t)))):
ifneq ($(ATM_SDK_TOOLS_IN_PATH),)
$(info Getting bin utils from $(BINUTILS_DIR_TXT))
else
	@echo
	@echo "ERROR: Failed to find $@"
	@echo
	@echo "ERROR: Toolchain not found - needs to be downloaded and extracted."
	@echo "ERROR: See $(TOOLS_DIR)/README for instructions."
	@echo
	@exit 1
endif

# FIXME: C removed due to problems on case-insensitive filesystems
CXX_EXT := cc cp cxx cpp c++

GDB_REMOTE ?= localhost:3333

RTT_PORT ?= 9090

OPENOCD_DIR := $(TOOLS_DIR)/openocd
OPENOCD_EXE := $(OPENOCD_DIR)/bin/$(OS)/openocd
OPENOCD_FLAGS += -s $(OPENOCD_DIR)/tcl -s $(OPENOCD_CFG_DIR)
OPENOCD_FLAGS += -f atm2x_openocd.cfg
ifneq ($(OPENOCD_DEBUG),)
OPENOCD_FLAGS += -d
endif
OPENOCD := $(OPENOCD_EXE) $(OPENOCD_FLAGS)

NVDS_TOOL := $(PLAT_TOOLS_DIR)/bin/$(OS)/nvds_tool

ATM_ISP := $(TOOLS_DIR)/atm_isp

IDE_PRJ_GEN := $(TOOLS_DIR)/ide_prj_gen/ide_prj_generator.py

.PHONY: check_openocd
check_openocd:
ifeq ($(OS),Linux)
	@if ! groups | grep -q plugdev; then \
	    echo; \
	    echo "ERROR: User $$USER is not a member of group plugdev."; \
	    echo "ERROR: See $(TOOLS_DIR)/README for instructions."; \
	    echo; \
	    exit 1; \
	fi
endif

.PHONY: openocd
openocd: check_openocd
	HTTP_KEEP_GOING=1 HTTP_NVDS_TOOL=$(NVDS_TOOL) HTTP_ATM_ISP=$(ATM_ISP) $(OPENOCD) -f servers.tcl

.PHONY: segger_rtt_server
segger_rtt_server: check_openocd
	SWDIF=JLINK $(OPENOCD) -c 'init; rtt setup 0x20014000 0x4000 "SEGGER RTT"; rtt start; rtt server start $(RTT_PORT) 0'

.PHONY: reset_target
reset_target: check_openocd
	FTDI_BENIGN_BOOT=1 FTDI_HARD_RESET=1 $(OPENOCD) -c "init; release_reset; sleep 100; set_normal_boot; exit"

ARCH_ATM := arch.atm

.PHONY: truncate_arch
truncate_arch:
	$(ATM_ISP) init -o $(ARCH_ATM) $(PLATFORM_FAMILY) $(PLATFORM_NAME)

ATM_ISP_LOAD = $(if $1,$(ATM_ISP) load$2 $1 -i $(ARCH_ATM) -o $(ARCH_ATM))

.PHONY: build_archive
build_archive: truncate_arch

.PHONY: show_archive
show_archive:
	$(ATM_ISP) decode -i $(ARCH_ATM)

burn_archive:
	$(ATM_ISP) burn -r $(realpath $(PLATFORM_DIR)/../../../) -i $(ARCH_ATM) $(if $(BURN_ARCH_VERIFY),-c) $(if $(BURN_ARCH_DEBUG),-v) $(if $(BURN_ARCH_ERASE_WORKAROUNDS),-e) $(if $(BURN_ARCH_PROGRAM_ONLY),-p) $(if $(BURN_ARCH_DEST_DIR),-d $(BURN_ARCH_DEST_DIR))

endif # __TOOLS_MK__
