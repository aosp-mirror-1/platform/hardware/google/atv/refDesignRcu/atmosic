################################################################################
#
# @file app.mk
#
# @brief Application make helper
#
# Copyright (C) Atmosic 2018-2022
#
################################################################################

ifndef __APP_MK__
__APP_MK__ = 1

APP ?= $(notdir $(CURDIR))

# Watchdog default 5 sec
WDOG ?= 5

LIBRARIES += atm_utils_c atm_utils_math
DRIVERS += \
	heap \
	hw_cfg \
	pinmux \
	rf \
	watchdog \

ifdef UART_FLASH
# default 32K NVDS
UART_FLASH_NVDS_SIZE ?= 0x8000
CFLAGS += -DUART_FLASH_NVDS_SIZE=$(UART_FLASH_NVDS_SIZE)

# UART_FLASH=0 for using UART0
# UART_FLASH=1 for using UART1
ifeq ($(UART_FLASH),0)
CFLAGS += -DCFG_UART_FLASH_UART0
else ifneq ($(UART_FLASH),1)
$(error "usage: make $(MAKECMDGOALS) UART_FLASH=<0|1>")
endif
DRIVERS += uart_flash
else ifndef RUN_IN_RAM
CFLAGS += -DCFG_EXT_FLASH
DRIVERS += ext_flash
endif # UART_FLASH

ifndef NO_PMU
DRIVERS += pmu reset
INCLUDES += $(DRIVER_DIR)/wurx
endif

# Enable Brownout by default
ifndef NO_BROWNOUT
DRIVERS += brwnout
endif

ifdef FORCE_LPC_RCOS
CFLAGS += -DFORCE_LPC_RCOS
endif

ifdef FPGA
DRIVERS += dtop_bypass
endif

# Include board specific pin mapping
CFLAGS += -DPINMAP_BOARD=$(BOARD)
INCLUDES += pinmap

# Code coverage for units under test
ifeq ($(TOOLSET),)
ifneq ($(and $(UU_TEST),$(filter -DAUTO_TEST,$(CFLAGS))),)
ifeq ($(filter -DIS_FOR_SIM,$(CFLAGS)),)
ifndef RUN_IN_RAM
COVERAGE ?= $(UU_TEST)
endif
endif # IS_FOR_SIM
endif # UU_TEST && AUTO_TEST
endif # TOOLSET

.PHONY: all
all: $(APP).bin $(APP).asm

include $(COMMON_USER_DIR)/tools.mk
include $(COMMON_USER_DIR)/nvds.mk
include $(if $(FLASHROM),$(LIB_DIR)/$(FLASHROM).mk,$(ROM_DIR)/rom_app.mk)

ifdef FLASHROM
override undefine USE_LIB
FLASHOFF ?= 0x10000000
endif

ifndef NO_USER_INIT
C_SRCS += $(USER_DIR)/user_init.c
endif

C_SRCS += $(wildcard $(APP).c) $(USER_DIR)/user_debug.c
CXX_SRCS += $(foreach e,$(CXX_EXT),$(wildcard $(APP).$e))

ifeq ($(TOOLSET),ARM)
C_SRCS += $(USER_DIR)/armcc/retarget.c
endif

ifdef DEBUG
C_SRCS += $(USER_DIR)/hardfault_handler_armv6m.c
DRIVERS += swd_dbg
ifndef SKIP_TB
DRIVERS += arm_traceback
endif # SKIP_TB
ifdef RTT_DBG
DRIVERS += SEGGER_RTT
CFLAGS += -DRTT_DBG
C_SRCS += $(DRIVER_DIR)/SEGGER_RTT/SEGGER_RTT_printf.c
C_SRCS += $(DRIVER_DIR)/SEGGER_RTT/rtt_debug.c
ifdef RTT_DBG_ONLY
CFLAGS += -DRTT_DBG_ONLY
endif
endif
else # DEBUG
ifneq (,$(filter -DAUTO_TEST,$(CFLAGS)))
C_SRCS += $(USER_DIR)/hardfault_handler_armv6m.c
ifndef SKIP_TB
DRIVERS += arm_traceback
endif
endif # AUTO_TEST
override undefine RTT_DBG
endif # DEBUG

ifdef COVERAGE
C_SRCS += $(USER_DIR)/user_gcov.c
endif

ifdef LPC_RCOS
DRIVERS += lpc_rcos
CFLAGS += -DLPC_RCOS
endif

LIBRARIES := $(sort $(LIBRARIES))

C_SRCS += $(addprefix $(ROM_DIR)/,$(ROM_ERRATA_SRC))

define include_driver
$(eval N := $(notdir $1))
$(eval D := $(DRIVER_DIR)/$1)
INCLUDES += $D
-include $D/$N.mk
ifeq ($$(lastword $$(MAKEFILE_LIST)),$D/$N.mk)
C_SRCS += $$(addprefix $D/,$$($N.C_SRCS))
INCLUDES += $$(addprefix $D/,$$($N.INCLUDES))
LIBS += $$(addprefix $D/,$$($N.LIBS))
CFLAGS += $$($N.CFLAGS)
else
C_SRCS += $D/$N.c
endif
endef
$(foreach d,$(DRIVERS),$(eval $(call include_driver,$d)))

define include_library
$(eval N := $(notdir $1))
$(eval L := $(LIB_DIR)/$1)
INCLUDES += $L
-include $L/$N.mk
ifeq ($$(lastword $$(MAKEFILE_LIST)),$L/$N.mk)
C_SRCS += $$(addprefix $L/,$$($N.C_SRCS))
INCLUDES += $$(addprefix $L/,$$($N.INCLUDES))
LIBS += $$(addprefix $L/,$$($N.LIBS))
CFLAGS += $$($N.CFLAGS)
else
C_SRCS += $(wildcard $L/$N*.c)
endif
endef
$(foreach l,$(LIBRARIES),$(eval $(call include_library,$l)))

EXCL_APP_LIB_ALL := $(EXCLUDE_APP_LIB_C_SRCS) $(APP).c
ifdef USE_LIB
C_SRCS := $(EXCL_APP_LIB_ALL)
else
S_SRCS += $(APP_USER_STARTUP)
endif

OBJS += $(notdir $(S_SRCS:%.s=%.o) $(C_SRCS:%.c=%.o) \
	$(foreach f,$(CXX_SRCS),$(basename $f).o))

# Non-conforming compilation units.  Needs to be cleaned up and removed.
FIXME_USES_BLE := \
	app_bass.c \
	app_diss.c \
	app_fio.c \
	app_gap.c \
	app_hrps.c \
	app_htpt.c \
	app_otaps.c \
	at_cmd_init.c \
	at_cmd_utils.c \
	atm_adv.c \
	atm_adv_param.c \
	atm_debug.c \
	atm_gap.c \
	atm_gap_param.c \
	atm_init.c \
	atm_init_param.c \
	atm_persync.c \
	atm_prfs.c \
	atm_prfs_task.c \
	atm_scan.c \
	atm_scan_param.c \
	bleadvdata.c \
	bleadvenable.c \
	bleadvlegacyparm.c \
	bleadvtxpwr.c \
	bleconntxpwr.c \
	blegapdevname.c \
	blegapdisconnect.c \
	blegapgetconnstat.c \
	blegapinit.c \
	blegapgetrssi.c \
	blegapgetpeerinfo.c \
	blegapcreateconn.c \
	blegapcancelcreateconn.c \
	blegapparnego.c \
	blescanenable.c \
	blescanfil.c \
	blescanfilrm.c \
	blegattaddchar.c \
	blegattadddesc.c \
	blegattadddescuserdfd.c \
	blegattaddservice.c \
	blegattdftchar1.c \
	blegattdftchar2.c \
	blegattdftserver.c \
	blegattdftservice.c \
	blegattmtuexchgreq.c \
	blegattmtuset.c \
	blegattdiscchar.c \
	blegattdiscchars.c \
	blegattdiscdescs.c \
	blegattdiscsvc.c \
	blegattdiscsvcs.c \
	blegattsvcactive.c \
	blegatttoble.c \
	blescanrspdata.c \
	blegapevtconn.c \
	blegapevtdisconn.c \
	blegapevtlinkinfo.c \
	blegapevtadvrpt.c \
	blegapevtpar.c \
	blegattmtuexchange.c \
	blegattread.c \
	blegattwrite.c \
	blegattnotify.c \
	blesmppairreq.c \
	blesmppairendind.c \
	sysbdaddr.c \
	sysfuncpin.c \
	syspm.c \
	sysnvds.c \
	blegattfromble.c \
	sysuart0raw.c \
	sysdfu.c \
	at_cmd_dfu_proc.c \

FIXME_EXAMPLE_USES_BLE := \
	atcmd_gap.c \
	ATM_ancsc.c \
	ATM_shub.c \
	attc_gatt.c \
	atts_gatt.c \
	atvrc_custom.c \
	BLE_adv.c \
	BLE_adv_scan.c \
	BLE_atcmd.c \
	BLE_att_client.c \
	BLE_att_server.c \
	BLE_bridge.c \
	BLE_harv_adv.c \
	BLE_scan_adv.c \
	BLE_scan.c \
	bridge_att.c \
	bridge_audio.c \
	bridge_gap.c \
	bridge_ir.c \
	bridge_mmi_client.c \
	bridge_mmi_server.c \
	button_demo.c \
	common.c \
	CT_adv.c \
	CT_button.c \
	CT_gatt.c \
	CT_nvds.c \
	CT_ota.c \
	CT_scan.c \
	CT_tracing.c \
	DTM.c \
	ESL_client.c \
	ESL_server.c \
	GPIO.c \
	HCI.c \
	HCI_vendor.c \
	HIB_restore.c \
	HID_keyboard.c \
	HID_mouse.c \
	HRP_sensor.c \
	HT_thermometer.c \
	ICM_sensor.c \
	kbd_gap.c \
	kbd_hogp.c \
	kbd_mmi.c \
	kbd_mmi_vkey.c \
	kbd_otaps.c \
	key_button.c \
	LECB_client.c \
	LECB_server.c \
	mbedtls_bist.c \
	mouse_hogp.c \
	mouse_gap.c \
	mouse_mmi.c \
	mouse_mmi_led.c \
	mouse_mmi_sensor.c \
	mouse_mmi_timer.c \
	mouse_otaps.c \
	per_sync.c \
	pm_demo.c \
	pvbcn_adv.c \
	pvbcn_gap.c \
	pvbcn_gatt.c \
	pvbcn_mmi.c \
	pvbcn_ota.c \
	PV_beacon.c \
	RAM_hibernate.c \
	rc_atvv.c \
	rc_gap.c \
	rc_hidau.c \
	rc_hogp.c \
	rc_ir.c \
	rc_mmi.c \
	rc_mmi_vkey.c \
	rc_ota.c \
	rc_pdm.c \
	rc_test_mode.c \
	RFsource_adv.c \
	RFsource_scan.c \
	swd_nego.c \
	tmp1075_sensor_adv.c \
	top_mmi.c \
	TPUTP_client.c \
	TPUTP_server.c \
	transceiver.c \
	uart0_raw_demo.c \
	vendor_specific.c \
	vkey_test.c \
	WURX_adv.c \
	WURX_scan_adv.c \

FIXME_DRIVER_USES_BLE := \
	atm_ble.c \
	atm_button.c \
	atm_pm.c \
	atm_vkey.c \
	bme680.c \
	dtop_bypass.c \
	ext_flash.c \
	gadc.c \
	hw_cfg.c \
	lpc_rcos.c \
	mouse.c \
	pmu.c \
	profiles.c \
	pseq.c \
	rf.c \
	shub.c \
	sw_event.c \
	sw_timer.c \
	trng.c \
	uart0.c \
	uart0_raw.c \
	uart_flash.c \
	uni_ir.c \
	wurx.c \

FRAMEWORK_UPPER_DIRS := $(wildcard $(LIB_DIR)/app_* $(LIB_DIR)/at_cmd* $(LIB_DIR)/atm_*)

ABSTRACT_FILES := \
    $(notdir $(shell find $(FRAMEWORK_UPPER_DIRS) -name '*.c')) \
    $(notdir $(shell find $(EXAMPLE_DIR) -name '*.c')) \
    $(notdir $(shell find $(DRIVER_DIR) -name '*.c')) \

FILTERED_ABSTRACT_FILES := $(filter-out $(FIXME_USES_BLE) \
    $(FIXME_EXAMPLE_USES_BLE) $(FIXME_DRIVER_USES_BLE),$(ABSTRACT_FILES))

INCLUDES := \
	$(TOP_DIR) \
	$(INCLUDE_DIR) \
	$(INCLUDE_DIR)/reg \
	$(INCLUDE_DIR)/reg_ble \
	$(INCLUDE_DIR)/arm \
	$(INCLUDE_DIR)/ble \
	$(DRIVER_DIR)/spi \
	$(DRIVER_DIR)/timer \
	$(INCLUDES)

BLE_ONLY_INCLUDES := \
	$(INCLUDE_DIR)/reg_ble \
	$(INCLUDE_DIR)/ble \
	$(LIB_DIR)/porting_ble \

EXT_INCLUDES := $(filter-out $(BLE_ONLY_INCLUDES),$(INCLUDES))

CFLAGS := \
	-D__MODULE__=\"$${<F}\" \
	-DFLASH_SIZE=$(if $(FLASH_SIZE),$(FLASH_SIZE),0x80000) \
	-DCORTEX_M0 \
	$(if $(FLASHROM),'-Dmain(...)=user_main(__VA_ARGS__)',-DCFG_USER) \
	$(if $(DEBUG),-DCFG_DBG) \
	$$(if $$(filter $(FILTERED_ABSTRACT_FILES),$$(<F)),\
	    $(EXT_INCLUDES:%=-I%),\
	    $($(if $(FLASHROM),FLASH)ROM_CFLAGS) $(INCLUDES:%=-I%)) \
	$(CFLAGS)

ifeq ($(TOOLSET),IAR)
ASFLAGS := \
	--cpu cortex-m0 --thumb -r \
	$(ASFLAGS)

CFLAGS := \
	--cpu cortex-m0 --thumb -Ohz \
	--vla \
	--debug \
	-e --warnings_are_errors \
	--diag_suppress Go029,Pa039,Pa082,Pa084,Pa089,Pa093,Pe111,Pe186,Pe188 \
	-I$(INCLUDE_DIR)/iccarm \
	$(CFLAGS)

DEP_FLAGS := --dependencies=n +

LDFLAGS := \
	--cpu cortex-m0 \
	-L$(USER_DIR) \
	$(if $(FLASH_SIZE),--config_def FLASH_SIZE=$(FLASH_SIZE)) \
	$(if $(USER_SIZE),--config_def USER_SIZE=$(USER_SIZE)) \
	$(if $(NVDS_SIZE),--config_def NVDS_SIZE=$(NVDS_SIZE)) \
	$(if $(URAM_START),--config_def URAM_START=$(URAM_START)) \
	$(if $(URAM_SIZE),--config_def URAM_SIZE=$(URAM_SIZE)) \
	$(if $(MPR_SIZE),--config_def MPR_SIZE=$(MPR_SIZE)) \
	--map=$(APP).map \
	$(LDFLAGS)
else
C_ONLY_FLAGS := \
	-Wstrict-prototypes \
	-Wold-style-definition \
	-Wmissing-prototypes \

comma := ,

ifeq ($(TOOLSET),ARM)
ASFLAGS := \
	--cpu=cortex-m0 --thumb -g \
	$(ASFLAGS)

C_ONLY_FLAGS += -std=gnu17

CFLAGS := \
	-Oz \
	-fshort-enums \
	$(CFLAGS)

LDFLAGS := -v \
	--cpu=cortex-m0 \
	$(if $(DEBUG),,--lto --lto_level=Oz) \
	--entry __main \
	--predefine="$(if $(FLASH_SIZE),-DFLASH_SIZE=$(FLASH_SIZE)) \
	    $(if $(USER_SIZE),-DUSER_SIZE=$(USER_SIZE)) \
	    $(if $(NVDS_SIZE),-DNVDS_SIZE=$(NVDS_SIZE)) \
	    $(if $(URAM_START),-DURAM_START=$(URAM_START)) \
	    $(if $(URAM_SIZE),-DURAM_SIZE=$(URAM_SIZE)) \
	    $(if $(MPR_SIZE),-DMPR_SIZE=$(MPR_SIZE))" \
	--map --list=$(APP).map \
	--info unused \
	$(LDFLAGS)
else
ASFLAGS := \
	-mcpu=cortex-m0 -mthumb -g \
	$(ASFLAGS)

C_ONLY_FLAGS += -std=c17 \
	-Wold-style-declaration \
	-Wmissing-parameter-type \

CFLAGS := \
	-Os \
	-Wformat-signedness -Wsuggest-attribute=noreturn \
	$(CFLAGS)

LDFLAGS := \
	-Wl,--fatal-warnings,--warn-common -mthumb -mcpu=cortex-m0 \
	$(if $(FLASHROM),,-nostartfiles) -Wl,--gc-sections \
	$(if $(COVERAGE),-fprofile-arcs) \
	$(if $(DEBUG),,-flto) \
	-L$(USER_DIR) \
	$(if $(FLASH_SIZE),-Wl$(comma)--defsym=FLASH_SIZE=$(FLASH_SIZE)) \
	$(if $(USER_SIZE),-Wl$(comma)--defsym=USER_SIZE=$(USER_SIZE)) \
	$(if $(NVDS_SIZE),-Wl$(comma)--defsym=NVDS_SIZE=$(NVDS_SIZE)) \
	$(if $(URAM_START),-Wl$(comma)--defsym=URAM_START=$(URAM_START)) \
	$(if $(URAM_SIZE),-Wl$(comma)--defsym=URAM_SIZE=$(URAM_SIZE)) \
	$(if $(MPR_SIZE),-Wl$(comma)--defsym=MPR_SIZE=$(MPR_SIZE)) \
	-Xlinker -Map=$(APP).map \
	$(LDFLAGS)
endif

CFLAGS := \
	-mcpu=cortex-m0 -mthumb -mlong-calls \
	-fms-extensions -ffunction-sections -fdata-sections \
	-fno-strict-aliasing -g3 $(if $(DEBUG),,-flto) \
	-Wall -Wchar-subscripts -Wformat \
	-Wuninitialized -Winit-self \
	-Wignored-qualifiers -Wswitch-default -Wswitch-enum -Wunused -Wundef \
	-Wnull-dereference -Wdouble-promotion \
	-Wwrite-strings -Wshadow -Wmissing-declarations -Wcast-qual \
	-Wpointer-arith \
	$(C_ONLY_FLAGS) -Werror \
	-I$(INCLUDE_DIR)/armgcc \
	$(CFLAGS)

CXXFLAGS := $(filter-out $(C_ONLY_FLAGS),$(CFLAGS)) -std=c++17
DEP_FLAGS := -MD
endif

ifdef USER_SIZE
CFLAGS += -DUSER_SIZE=$(USER_SIZE)
endif

ifdef MPR_SIZE
CFLAGS += -DMPR_SIZE=$(MPR_SIZE)
endif

ifdef PMU_CFG
CFLAGS += -D$(PMU_CFG)
ifeq ($(filter-out VBAT_GT_1p8V_VDDIO_EXT VBAT_GT_1p8V_VDDIO_INT VBAT_LE_1p8V NO_VBAT,$(PMU_CFG)),)
else
$(warning Unknown PMU_CFG $(PMU_CFG))
endif
endif # PMU_CFG

define o_from_s_RULE
$(notdir $(1:%.s=%.o)): $(1) $(ASSEMBLE)
	$(ASSEMBLE) $(ASFLAGS) $$< -o $$@
endef
$(foreach src,$(S_SRCS),$(eval $(call o_from_s_RULE,$(src))))

define o_from_c_RULE
$(notdir $(1:%.c=%.o)): $(1) $(firstword $(COMPILE)) \
    | c_bindings
	$(COMPILE) $(CFLAGS) $$(if $$(filter $(COVERAGE),\
	    $$(basename $$@)),-fprofile-arcs -ftest-coverage) \
	    $(DEP_FLAGS) -c $$< -o $$@
ifneq (,$(filter -DAUTO_TEST,$(CFLAGS)))
	-$$(if $$(filter $(ABSTRACT_FILES),$$(<F)),\
	    ! grep $$(addprefix -e ,$(BLE_ONLY_INCLUDES)) $$(@:%.o=%.d))
endif
endef
$(foreach src,$(C_SRCS),$(eval $(call o_from_c_RULE,$(src))))

define o_from_cxx_RULE
$(notdir $(basename $1).o): $(1) $(firstword $(CXX)) \
    | cpp_bindings
	$(CXX) $(CXXFLAGS) $$(if $$(filter $(COVERAGE),\
	    $$(basename $$@)),-fprofile-arcs -ftest-coverage) \
	    $(DEP_FLAGS) -c $$< -o $$@
endef
$(foreach src,$(CXX_SRCS),$(eval $(call o_from_cxx_RULE,$(src))))

-include *.d

.PHONY: c_bindings cpp_bindings
c_bindings cpp_bindings::

ifdef RUST_LIBS
RUST_TARGET := thumbv6m-none-eabi

define lib_from_rust_TEMPLATE
$(eval A := $1/target/$(RUST_TARGET)/$(if $(DEBUG),debug,release)/lib$1.rlib)
OBJS += $A
.PHONY: $A
$A:
	$(if $(wildcard $1_wrapper.h),bindgen --use-core --ctypes-prefix cty \
	    -o $1_bindings.rs $1_wrapper.h -- \
	    --sysroot=$(BINUTILS_DIR)/../arm-none-eabi \
	    -target $(RUST_TARGET) $(filter-out -W%,$(CFLAGS)))
	cd $1 && cargo build --target $(RUST_TARGET) $(if $(DEBUG),,--release)
c_bindings::
	cbindgen -l c -o $1_bindings.h $1
cpp_bindings::
	cbindgen -l c++ -o $1_bindings.hpp $1
clean::
	cd $1 && cargo clean
	rm -f $1/Cargo.lock
	rm -f $1_bindings.rs $1_bindings.h $1_bindings.hpp
$(eval $(if $(filter undefine,$(.FEATURES)),undefine A,A :=))
endef # lib_from_rust_TEMPLATE
$(foreach l,$(RUST_LIBS),$(eval $(call lib_from_rust_TEMPLATE,$l)))
endif # RUST_LIBS

ifneq (,$(strip $(CXX_SRCS)))
LINK := $(CXX)
endif

EXCLUDE_APP_SRC=$(notdir $(EXCL_APP_LIB_ALL))
EXCLUDE_APP_OBJ+=$(patsubst %.c,%.o,$(EXCLUDE_APP_SRC))

ifdef FLASHROM
$(APP).elf: $(OBJS) $(firstword $(LINK))
	$(OBJCOPY) $(if $(COVERAGE),--redefine-syms $(ROM_DIR)/gcov.redef) \
	    --redefine-syms $(ROM_DIR)/user.redef \
	    $(LIB_DIR)/lib$(FLASHROM)$(if $(DEBUG),_dbg).a flashrom.a
	$(LINK) -Tcmsdk_cm0_flash_rom.ld $(LDFLAGS) $(OBJS) $(LIBS) -o $@ \
	    -Wl,-whole-archive flashrom.a -Wl,-no-whole-archive
else # FLASHROM
$(LIB_DIR)/app_lib/$(APP)$(if $(DEBUG),_dbg).a: $(OBJS)
	mkdir -p $(@D)
	$(AR) rcD --plugin=$(PLUGIN) $@ $(filter-out $(EXCLUDE_APP_OBJ),$(OBJS))
	$(RANLIB) -D --plugin=$(PLUGIN) $@
	$(AR) t $@

$(APP).elf: $(OBJS) $(firstword $(LINK)) \
	$(if $(BUILD_APP_LIB),$(LIB_DIR)/app_lib/$(APP)$(if $(DEBUG),_dbg).a)
ifeq ($(MAKECMDGOALS),build_info)
	@echo IDE_LINK_CMD:
endif
	$(LINK) $(LDFLAGS) $(OBJS) $(LIBS) -o $@
endif # FLASHROM

ifeq ($(TOOLSET),ARM)
$(APP).bin: $(APP).elf
	$(ARM_DIR)/fromelf --bin --output=$@ $<

$(APP).ihex: $(APP).elf
	$(ARM_DIR)/fromelf --i32 --output=$@ $<
else
$(APP).bin: $(APP).elf $(OBJCOPY)
	$(OBJCOPY) -O binary $< $@

$(APP).ihex: $(APP).elf $(OBJCOPY)
	$(OBJCOPY) -O ihex $< $@
endif

$(APP).bo: $(APP).bin $(OBJCOPY)
	$(OBJCOPY) -I binary -O elf32-littlearm -B arm --rename-section .data=.rodata,alloc,load,readonly,data,contents $< $@

$(APP).hex: $(APP).ihex $(OBJCOPY)
	$(OBJCOPY) -O verilog $< $@

$(APP).asm: $(APP).elf $(OBJDUMP)
	$(OBJDUMP) -d $< >$@

.PHONY: program
program: $(APP).elf reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_load_flash $< $(if $(FLASH_START),$(FLASH_START) $(if $(NVDS_START),$(NVDS_START))); exit"

.PHONY: program_all
program_all: $(if $(flash_nvds.data),build_flash_nvds push_flash_nvds) program

.PHONY: verify
verify: $(APP).elf reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_verify_flash $<; exit"

.PHONY: run.ram
run.ram: $(APP).bin reset_target
	$(eval RUN_IN_RAM_HOOK:=$(shell $(NM) rom.elf | grep '\<run_in_ram_hook\>' | awk '{print "0x" $$1}'))\
	$(OPENOCD) -c "init; verify_rom_version; catch {sydney_erase_flash}; load_ram_image $(RUN_IN_RAM_HOOK) $< $(if $(URAM_START),$(URAM_START),0x20014000); resume $(if $(URAM_START),$(URAM_START),0x20014000); exit"

.PHONY: run.flash
run.flash: $(APP).elf reset_target
	$(OPENOCD) -c "init; verify_rom_version; sydney_load_flash $< \
		$(if $(FLASH_START),$(FLASH_START) $(if $(PRESERVE_USER),$(USER_START),$(if $(NVDS_START),$(NVDS_START)) $(if $(FLASHOFF),$(FLASHOFF)))); set _RESET_HARD_ON_EXIT 1; exit"

.PHONY: run
run: run.$(if $(RUN_IN_RAM),ram,flash)

.PHONY: run_all
run_all: $(if $(flash_nvds.data),build_flash_nvds push_flash_nvds) run

.PHONY: pull_core
pull_core: check_openocd
	$(OPENOCD) -c "init; get_coredump; exit"

.PHONY: gcov
gcov:
	$(GCOV) -ab *.gcda

.PHONY: gdb
gdb: $(if $(NO_REBUILD),,$(APP).elf) $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/atmx2.gdb $(APP).elf $(GDB_EXTRA)

.PHONY: gdb_target
gdb_target: $(if $(NO_REBUILD),,$(APP).elf) $(GDB)
	$(GDB) -x $(GDB_CFG_DIR)/atmx2.gdb $(APP).elf $(GDB_EXTRA) -ex "target remote $(GDB_REMOTE)"

.PHONY: keil_gcc_gen
keil_gcc_gen:
	python3 $(IDE_PRJ_GEN) $(CURDIR) -s ATMx2 -m $(MAKEOVERRIDES)

.PHONY: keil_arm_gen
keil_arm_gen:
	python3 $(IDE_PRJ_GEN) $(CURDIR) -s ATMx2 -i keil_arm -m $(MAKEOVERRIDES)

.PHONY: ses_gen
ses_gen:
	python3 $(IDE_PRJ_GEN) $(CURDIR) -s ATMx2 -i ses -m $(MAKEOVERRIDES)

.PHONY: iar_gen
iar_gen:
	python3 $(IDE_PRJ_GEN) $(CURDIR) -s ATMx2 -i iar -m $(MAKEOVERRIDES)

.PHONY: build_info
build_info: all
	@echo IDE_S_SRCS = $(S_SRCS)
	@echo IDE_C_SRCS = $(C_SRCS)
	@echo IDE_ASFLAGS = $(ASFLAGS)
	@echo IDE_CFLAGS = $(CFLAGS)
	@echo IDE_LIBS = $(LIBS)

.PHONY: clean
clean::
	rm -f $(APP).{bin,elf,asm}
	rm -f flashrom.a
	-rm -f *.d *.o
	-rm -f *.gcno *.gcda
ifndef COVERAGE
	-rm -f *.gcov
endif
	-rm -f *.map

ARCH_FLASH_TYPE := bin

ifeq ($(ARCH_FLASH_TYPE),bin)
# TODO: figure out a solution to avoid duplicating these platform-dependent
# values from <PLATFORM_DIR>/openocd/*_flash.tcl in Make.  May require a change
# in archive format.
ATM_ISP_LOAD_FLASH_XARGS := \
	$(if $(FLASH_START),$(FLASH_START),0x0) \
	$(if $(NVDS_START),$(NVDS_START),0x78000) \
	0x10000000 \
	$(if $(MPR_START), -mpr_start $(MPR_START)) \
	$(if $(MPR_SIZE), -mpr_size $(MPR_SIZE)) \
	$(if $(MPR_LOCK_SIZE), -mpr_lock_size $(MPR_LOCK_SIZE))
else ifeq ($(ARCH_FLASH_TYPE),elf)
ATM_ISP_LOAD_FLASH_XARGS := $(if $(FLASH_START),$(FLASH_START) $(if $(NVDS_START),$(NVDS_START)))
else
$(error Unknown ARCH_FLASH_TYPE $(ARCH_FLASH_TYPE))
endif

.PHONY: arch_add_flash
arch_add_flash: $(APP).$(ARCH_FLASH_TYPE)
	$(call ATM_ISP_LOAD,$< $(ATM_ISP_LOAD_FLASH_XARGS),Flash)

build_archive: arch_add_flash

.PHONY: info
info: $(APP).elf $(SIZE)
	$(SIZE) $<

include $(COMMON_USER_DIR)/layout_info.mk

endif # __APP_MK__
