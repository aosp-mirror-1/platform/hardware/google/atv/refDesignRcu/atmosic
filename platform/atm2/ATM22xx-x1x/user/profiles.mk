################################################################################
#
# @file profiles.mk
#
# @brief Common BLE profile build settings
#
# Copyright (C) Atmosic 2020-2021
#
################################################################################

ifndef __PROFILE_MK__
__PROFILE_MK__ = 1

ifdef PROFILES
# Enable the profiles framework
DRIVERS += profiles

# $1 - common part of profile name
# $2 - client/server part of profile name
# $3 - common directory ($1 if empty)
# $4 - define (uppercase $1$2 if empty)
define profile_TEMPLATE
$(eval A := $(if $3,$3,$1))
$(eval B := $(if $4,$4,$(call to_upper,$1$2)))
ifneq ($(filter $B,$(PROFILES)),)
C_SRCS += $(BLE_PRF_DIR)/$A/$1$2/src/$1$2.c \
    $(wildcard $(BLE_PRF_DIR)/$A/$1$2/src/$1$2_task.c)
INCLUDES += $(BLE_PRF_DIR)/$A $(BLE_PRF_DIR)/$A/$1$2/api
CFLAGS += -DCFG_PRF_$B
endif
$(eval $(if $(filter undefine,$(.FEATURES)),undefine A,A :=))
$(eval $(if $(filter undefine,$(.FEATURES)),undefine B,B :=))
endef


#
# Standard profiles
#

# ANCSC Apple Notification Center Service Client
$(eval $(call profile_TEMPLATE,ancs,c))

# ANPC Alert Notification Client
$(eval $(call profile_TEMPLATE,anp,c))
# ANPS Alert Notification Server
$(eval $(call profile_TEMPLATE,anp,s))

# BASC Battery Service Client
$(eval $(call profile_TEMPLATE,bas,c))
# BASS Battery Service Server
$(eval $(call profile_TEMPLATE,bas,s))

# BLPC Blood Pressure collector
$(eval $(call profile_TEMPLATE,blp,c))
# BLPS Blood Pressure Sensor
$(eval $(call profile_TEMPLATE,blp,s))

# BCSC Body Composition Client
$(eval $(call profile_TEMPLATE,bcs,c))
# BCSS Body Composition Server
$(eval $(call profile_TEMPLATE,bcs,s))

# CGMC Continuous Glucose Monitoring Client
$(eval $(call profile_TEMPLATE,cgm,c))
# CGMS Continuous Glucose Monitoring Server
$(eval $(call profile_TEMPLATE,cgm,s))

# CPPC Cycling Power Client
$(eval $(call profile_TEMPLATE,cpp,c))
# CPPS Cycling Power Server
$(eval $(call profile_TEMPLATE,cpp,s))

# CSCPC Cycling Speed and Cadence Client
$(eval $(call profile_TEMPLATE,cscp,c))
# CSCPS Cycling Speed and Cadence Server
$(eval $(call profile_TEMPLATE,cscp,s))

# DISC Device Information Service Client
$(eval $(call profile_TEMPLATE,dis,c))
# DISS (Device Information Service) Server
$(eval $(call profile_TEMPLATE,dis,s))

# ENVC Environmental Sensing Client
$(eval $(call profile_TEMPLATE,env,c))
# ENVS Environmental Sensing Server
$(eval $(call profile_TEMPLATE,env,s))

# FMPL Find me locator
$(eval $(call profile_TEMPLATE,find,l,,FMPL))
# FMPT Find me target
$(eval $(call profile_TEMPLATE,find,t,,FMPT))

# GLPC Glucose Collector
$(eval $(call profile_TEMPLATE,glp,c))
# GLPS Glucose Sensor
$(eval $(call profile_TEMPLATE,glp,s))

# HOGPD HID over GATT Profile Device
$(eval $(call profile_TEMPLATE,hogp,d))
# HOGPBH HID Profile Boot Host
$(eval $(call profile_TEMPLATE,hogp,bh))
# HOGPRH HID over GATT Report Profile Host
$(eval $(call profile_TEMPLATE,hogp,rh))

# HRPC Heart Rate collector
$(eval $(call profile_TEMPLATE,hrp,c))
# HRPS Heart Rate Sensor
$(eval $(call profile_TEMPLATE,hrp,s))

# HTPC Health thermometer collector
$(eval $(call profile_TEMPLATE,htp,c))
# HTPT Health Thermometer
$(eval $(call profile_TEMPLATE,htp,t))

# IPSC Internet Protocol Support Profile Client
$(eval $(call profile_TEMPLATE,ips,c))
# IPSS Internet Protocol Support Profile Server
$(eval $(call profile_TEMPLATE,ips,s))

# LANC Location and Navigation Client
$(eval $(call profile_TEMPLATE,lan,c))
# LANS Location and Navigation Server
$(eval $(call profile_TEMPLATE,lan,s))

# PASPC Phone Alert Status Client
$(eval $(call profile_TEMPLATE,pasp,c))
# PASPS Phone Alert Status Server
$(eval $(call profile_TEMPLATE,pasp,s))

# PLXC Pulse Oximeter Profile Client
$(eval $(call profile_TEMPLATE,plx,c))
# PLXS Pulse Oximeter Profile Server
$(eval $(call profile_TEMPLATE,plx,s))

# PXPM Proximity monitor
$(eval $(call profile_TEMPLATE,prox,m,,PXPM))
# PXPR Proximity reporter
$(eval $(call profile_TEMPLATE,prox,r,,PXPR))

# RSCPC Running Speed and Cadence Client
$(eval $(call profile_TEMPLATE,rscp,c))
# RSCPS Running Speed and Cadence Server
$(eval $(call profile_TEMPLATE,rscp,s))

# SCPPC Scan Parameter Client
$(eval $(call profile_TEMPLATE,scpp,c))
# SCPPS Scan Parameter Server
$(eval $(call profile_TEMPLATE,scpp,s))

# TIPC Time Client
$(eval $(call profile_TEMPLATE,tip,c))
# TIPS Time Server
$(eval $(call profile_TEMPLATE,tip,s))

# UDSC User Data Service Client
$(eval $(call profile_TEMPLATE,uds,c))
# UDSS User Data Service Server
$(eval $(call profile_TEMPLATE,uds,s))

# WPTC Wireless Power Transfer Profile Client
$(eval $(call profile_TEMPLATE,wpt,c))
# WPTS Wireless Power Transfer Profile Server
$(eval $(call profile_TEMPLATE,wpt,s))

# WSCC Weight Scale Client
$(eval $(call profile_TEMPLATE,wsc,c,wscp))
# WSCS Weight Scale Server
$(eval $(call profile_TEMPLATE,wsc,s,wscp))

#
# Atmosic profiles
#

# AGPC Atmosic Generic Client
$(eval $(call profile_TEMPLATE,agp,c))
# AGPS Atmosic Generic Client
$(eval $(call profile_TEMPLATE,agp,s))

# ATVVS (Android TV Voice) Service
$(eval $(call profile_TEMPLATE,atvv,s))

# DTPS (Data Transfer Profile) Server
ifneq ($(filter DTPS,$(PROFILES)),)
C_SRCS += $(BLE_PRF_DIR)/dts/dts/src/dtps.c
CFLAGS += -DCFG_PRF_DTPS
DTS_SERVER_ROLE := 1
endif # DTPS

# OTAPS (Over the Air Upgrade Profile) Server
$(eval $(call profile_TEMPLATE,otap,s))
ifneq ($(filter OTAPS,$(PROFILES)),)
CFLAGS += -DCFG_CRC
LIBRARIES += upgrade crc
DTS_SERVER_ROLE := 1
ifdef OTA_DISCON_BEFORE_REBOOT
CFLAGS += -DCFG_UPGD_REBOOT_POSTPONED
endif
endif # OTAPS

# TPUTPC Throughput Testing Client
$(eval $(call profile_TEMPLATE,tputp,c))
ifneq ($(filter TPUTPC,$(PROFILES)),)
DTS_CLIENT_ROLE := 1
DTS_UTILS := 1
endif # TPUTPC

# TPUTPS Throughput Testing Server
$(eval $(call profile_TEMPLATE,tputp,s))
ifneq ($(filter TPUTPS,$(PROFILES)),)
DTS_SERVER_ROLE := 1
DTS_UTILS := 1
endif # TPUTPS

# DTSS (Data Transfer Service) server role library
ifdef DTS_SERVER_ROLE
# DTS server role support
C_SRCS += $(BLE_PRF_DIR)/dts/dts/src/dts_server.c
INCLUDES += $(BLE_PRF_DIR)/dts/dts/api
endif  # DTS_SERVER_ROLE

# DTSC (Data Transfer Service) client role library
ifdef DTS_CLIENT_ROLE
C_SRCS += $(BLE_PRF_DIR)/dts/dts/src/dts_client.c
INCLUDES += $(BLE_PRF_DIR)/dts/dts/api
endif # DTS_CLIENT_ROLE

# DTS utility library
ifdef DTS_UTILS
C_SRCS += $(BLE_PRF_DIR)/dts/dts/src/dts_utils.c
endif # DTS_UTILS

ifneq (,$(or $(filter ANCSC BASS DISS HTPT OTAPS,$(PROFILES)),$(DTS_CLIENT_ROLE)))
LIBRARIES += prf
endif

endif # PROFILES

endif # __PROFILE_MK__
