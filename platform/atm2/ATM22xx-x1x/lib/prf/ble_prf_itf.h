/**
 *******************************************************************************
 *
 * @file ble_prf_itf.h
 *
 * @brief BLE profile interface
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_PRFITF Profile interface
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework profile interface
 *
 * This module contains the profile interface definitioin for middleware and
 * application layer.
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDE FILE
 *******************************************************************************
 */
#include "ble_gap.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * STRUCTURE
 *******************************************************************************
 */
/// The profile interface callback API for middleware and application layer.
typedef struct {
    /// @brief Profile initialization.
    /// @details This function will be called before BT controller reset.
    void (*init_op)(void);
    /// @brief Profile start.
    /// @details This function will be called after BT controller reset.
    /// Profile shall add service in this function if needed.
    /// @param[in] cb Callback needed be called after starting duties completed.
    /// @param[in] ctxt Context data.
    void (*start_op)(ble_gapm_cmd_cb cb, void const *ctxt);
} ble_prf_cbs_t;

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_PRFITF

