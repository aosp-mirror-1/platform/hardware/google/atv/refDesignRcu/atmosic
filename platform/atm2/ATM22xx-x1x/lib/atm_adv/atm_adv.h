/**
 *******************************************************************************
 *
 * @file atm_adv.h
 *
 * @brief Atmosic BLE advertising procedure
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_PADV ADV procedures
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework advertisement procedures
 *
 * This module contains the necessary procedure to manipulate ADV.
 *
 * @{
 *******************************************************************************
 */

#include "ble_gap.h"
#include "atm_debug.h"
#include "atm_adv_param.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ATM_INVALID_ADVIDX 0xFF

/// ADV state.
typedef enum {
    /// Advertising state machine is in idle state.
    ATM_ADV_IDLE,
    /// Advertising state machine is in creating state.
    ATM_ADV_CREATING,
    /// Successfully created, data not set.
    ATM_ADV_CREATED,
    /// ADV data is under setting.
    ATM_ADV_ADVDATA_SETTING,
    /// ADV data is set.
    ATM_ADV_ADVDATA_DONE,
    /// Scan data is under setting.
    ATM_ADV_SCANDATA_SETTING,
    /// Scan data is set.
    ATM_ADV_SCANDATA_DONE,
    /// Advertising state machine is in off state.
    ATM_ADV_OFF,
    /// Advertising state machine is in starting state.
    ATM_ADV_STARTING,
    /// Advertising state machine is in on state.
    ATM_ADV_ON,
    /// Advertising state machine is in stoping state.
    ATM_ADV_STOPPING,
    /// Advertising state machine is in deleting state.
    ATM_ADV_DELETING,
    /// Delete created advertising successfully.
    ATM_ADV_DELETED
} atm_adv_state_t;

/// Advertising callback function
typedef void (*atm_adv_cb_t)(atm_adv_state_t state, uint8_t act_idx,
    ble_err_code_t status);

/**
 *******************************************************************************
 * @brief Register advertising callback function
 * @param[in] cb Advertising callback function.
 *******************************************************************************
 */
void atm_adv_reg(atm_adv_cb_t cb);

/**
 *******************************************************************************
 * @brief Apply the advertising activity
 * @param[in] create Advertising activity
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_create(atm_adv_create_t const *create);

/**
 *******************************************************************************
 * @brief Set the advertising data
 * @param[in] act_idx Activity index
 * @param[in] data Advertising data
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_set_adv_data(uint8_t act_idx, atm_adv_data_t const *data);

/**
 *******************************************************************************
 * @brief Set the scan response data
 * @param[in] act_idx Activity index
 * @param[in] data Scan response data
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_set_scan_data(uint8_t act_idx, atm_adv_data_t const *data);

/**
 *******************************************************************************
 * @brief Start advertising
 * @param[in] act_idx Activity index
 * @param[in] start Additional advertising parameters
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_start(uint8_t act_idx, atm_adv_start_t const *start);

/**
 *******************************************************************************
 * @brief Stop advertising
 * @param[in] act_idx Activity index
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_stop(uint8_t act_idx);

/**
 *******************************************************************************
 * @brief Delete advertising
 * @param[in] act_idx Activity index
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_delete(uint8_t act_idx);

/**
 *******************************************************************************
 * @brief Get advertising activity state
 * @param[in] act_idx Activity index
 * @return state value @ref atm_adv_state_t
 *******************************************************************************
 */
uint8_t atm_adv_get_state(uint8_t act_idx);

/**
 *******************************************************************************
 * @brief Check information element
 *
 * @param[in] payload The information element of adv. data or scan response data
 * @param[in] len The length of payload
 * @return true if data is correct
 *******************************************************************************
 */
__NONNULL(1)
bool atm_adv_check_adv_len_valid(uint8_t const *payload, uint16_t len);

/**
 *******************************************************************************
 * @brief Parameter sanity check for adv data
 * @param[in] create Parameter setting for create adv.
 * @param[in] adv Advertising data
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_check_adv_data(atm_adv_create_t const *create,
    atm_adv_data_t const *adv);

/**
 *******************************************************************************
 * @brief Parameter sanity check for scan response data
 * @param[in] create Parameter setting for create adv.
 * @param[in] scan Scan response data
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_check_scan_resp_data(atm_adv_create_t const *create,
    atm_adv_data_t const *scan);

/**
 *******************************************************************************
 * @brief Parameter sanity check for set adv. data or scan response data
 * @param[in] create Parameter setting for create adv.
 * @param[in] adv Advertising data
 * @param[in] scan Scan response data
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
ble_err_code_t atm_adv_set_data_sanity(atm_adv_create_t const *create,
    atm_adv_data_t const *adv, atm_adv_data_t const *scan);

/**
 *******************************************************************************
 * @brief Advertisement stopped indication callback function
 * @param[in] param Indication information
 *******************************************************************************
 */
void atm_adv_stopped_ind(ble_gap_ind_stop_t const *param);

/**
 *******************************************************************************
 * @brief Convert data type from atm_adv_data_nvds_t to atm_adv_data_t
 * @param[in] data Flash nvds data pointer reading from flash nvds
 * @return data pointer using atm_adv_data_t data type
 *******************************************************************************
 */
__INLINE atm_adv_data_t *atm_adv_convert_nvds_data_type(atm_adv_data_nvds_t *data)
{
    return (atm_adv_data_t *)data;
}

/**
 *******************************************************************************
 * @brief Print advertising timeout parameter
 * @param[in] create Parameter setting for create adv.
 * @param[in] start Parameter setting for start adv.
 * @return BLE_ERR_NO_ERROR for success @ref ble_err_code_t
 *******************************************************************************
 */
#ifdef ATM_DEBUG
ble_err_code_t atm_adv_timeout_param_print(atm_adv_create_t const *create,
    atm_adv_start_t const *start);
#else
__INLINE uint8_t atm_adv_timeout_param_print(atm_adv_create_t const *create,
    atm_adv_start_t const *start)
{
    return BLE_ERR_NO_ERROR;
}
#endif

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_PADV

