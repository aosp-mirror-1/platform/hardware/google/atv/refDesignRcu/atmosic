/**
 *******************************************************************************
 *
 * @file ble_diss.h
 *
 * @brief Device Information Service Server Module
 *
 * Copyright (C) Atmosic 2020-2021
 *
 *******************************************************************************
 */

#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_DISS Device information service procedures
 * @ingroup ATM_BTFM_API
 * @brief BLE bluetooth device information service procedures
 *
 * This module contains the necessary procedure to send the device information.
 *
 * @{
 *******************************************************************************
 */
/*
 * INCLUDE FILES
 *******************************************************************************
 */
#include <stdint.h>
#include "ble_gap.h"
#include "ble_internal.h"
#include "ble_prf_itf.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINITION
 *******************************************************************************
 */
#define BLE_DISS_MODULE_NAME       "kdiss"

/*
 * GLOBAL VARIABLES DECLARATION
 *******************************************************************************
 */

/*
 * DEFINES
 *******************************************************************************
 */
struct diss_param {
    /// Security Level :
    ///  7    6    5    4    3    2    1    0
    /// +----+----+----+----+----+----+----+----+
    /// |   Reserved   |DIS |  AUTH   |EKS | MI |
    /// +----+----+----+----+----+----+----+----+
    ///
    /// - MI: 1 - Application task is a Multi-Instantiated task, 0 - Mono-Instantiated
    /// Only applies for service - Ignored by collectors:
    /// - EKS: Service needs a 16 bytes encryption key
    /// - AUTH: 0 - Disable, 1 - Enable, 2 - Unauth, 3 - Auth
    /// - DIS: Disable the service
    uint8_t sec_lvl;
    /// Supported Feature configuration
    uint16_t feature;
    void (*cb_value_req)(uint8_t, uint8_t const **, uint8_t *);
};

typedef const ble_prf_cbs_t *(ble_prf_get_cbs_t)(struct diss_param const *init);

#ifdef __cplusplus
}
#endif

/// @} ATM_BTFM_DISS
