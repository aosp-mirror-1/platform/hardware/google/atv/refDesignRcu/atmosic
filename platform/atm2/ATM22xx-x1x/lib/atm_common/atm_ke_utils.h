/**
 *******************************************************************************
 *
 * @file atm_ke_utils.h
 *
 * @brief ATM bluetooth framework common utility functions
 *
 * Copyright (C) Atmosic 2020-2023
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @addtogroup ATM_CO_UTILS
 * @brief ATM bluetooth framework common kernel functions
 *
 * This module contains the common kernel functions and macros
 * used by the bluetooth framework API.
 *
 * @{
 *******************************************************************************
 */
#include "rwip_config.h"
#include "co_version.h"
#include <string.h>
#include "ke_mem.h"
#include "ke_msg.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 *******************************************************************************
 * @brief Allocation of a block of memory and sets allocated memory to zero
 * @param[in] nitems number of elements to be allocated.
 * @param[in] size Size of the memory area that need to be allocated.
 * @param[in] type Type of memory block
 *
 * @return A pointer to the allocated memory area.
 *******************************************************************************
 */
__INLINE void *atm_ke_calloc(uint32_t nitems, uint32_t size, uint8_t type)
{
    return memset(ke_malloc(nitems * size, type), 0, nitems * size);
}

#if (RWBT_SW_VERSION_MAJOR < 11)
#define ATM_KE_MSG_CALLOC(id, dest, src, param_str) \
    KE_MSG_ALLOC(id, dest, src, param_str)

#define ATM_KE_MSG_CALLOC_DYN(id, dest, src, param_str, length) \
    KE_MSG_ALLOC_DYN(id, dest, src, param_str, length)
#else // (RWBT_SW_VERSION_MAJOR < 11)
#define ATM_KE_MSG_CALLOC(id, dest, src, param_str) \
    (struct param_str *)memset(KE_MSG_ALLOC(id, dest, src, param_str), 0, \
	sizeof(struct param_str))

#define ATM_KE_MSG_CALLOC_DYN(id, dest, src, param_str, length) \
    (struct param_str *)memset(KE_MSG_ALLOC_DYN(id, dest, src, param_str, \
	length), 0, sizeof(struct param_str) + length)
#endif // (RWBT_SW_VERSION_MAJOR < 11)

#ifdef __cplusplus
}
#endif

/// @} ATM_CO_UTILS
