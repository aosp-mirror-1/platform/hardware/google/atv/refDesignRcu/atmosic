/**
 *******************************************************************************
 *
 * @file atm_co_bt_defines.h
 *
 * @brief ATM bluetooth framework common Bluetooth defines.
 *
 * Copyright (C) Atmosic 2021
 *
 *******************************************************************************
 */

#pragma once

/**
 *******************************************************************************
 * @addtogroup ATM_CO_BT_DEFINES
 * @brief ATM bluetooth framework common Bluetooth defines.
 * @{
 *******************************************************************************
 */

#define ATMOSIC_COMPANY_ID 0x240A

/// @} ATM_CO_BT_DEFINES
