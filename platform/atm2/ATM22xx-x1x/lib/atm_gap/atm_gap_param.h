/**
 *******************************************************************************
 *
 * @file atm_gap_param.h
 *
 * @brief Header file - ATM bluetooth framework GAP parameters
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

#include "atm_log.h"
#include "atm_gap.h"

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_GAPP GAP parameters
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework GAP parameters
 *
 * This module contains the necessary function to provide predefined parameters
 * for usage of
 * @ref atm_gap_start.
 *
 * @{
 *******************************************************************************
 */

// overwritten by application
#ifdef GAP_PARM_NAME
#include STR(GAP_PARM_NAME)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CFG_GAP_MAX_LINKS
#define CFG_GAP_MAX_LINKS 1
#endif

// Max no. of LECB connections
#ifndef CFG_GAP_MAX_LECB
#define CFG_GAP_MAX_LECB 1
#endif

#ifndef CFG_GAP_TEST_TX_PWR_LVL
#define CFG_GAP_TEST_TX_PWR_LVL BLE_GAP_MAX_TEST_TX_PWR_LVL
#endif

// Minimum length of connection event in 0.625 ms
#ifndef CFG_GAP_PARAM_UPDATE_CE_MIN
#define CFG_GAP_PARAM_UPDATE_CE_MIN 0
#endif

// Maximum length of connection event in 0.625 ms
#ifndef CFG_GAP_PARAM_UPDATE_CE_MAX
#define CFG_GAP_PARAM_UPDATE_CE_MAX 0
#endif

/**
 *******************************************************************************
 * @brief Get persistent GAP start parameter.
 * @return Pointer of GAP start parameter
 *******************************************************************************
 */
__ATM_GAP_PARAM_CONST atm_gap_param_t *atm_gap_param_get(void);

#ifdef __cplusplus
}
#endif

/// @} ATM_BTFM_GAPP
