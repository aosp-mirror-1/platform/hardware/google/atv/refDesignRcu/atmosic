/**
 *******************************************************************************
 *
 * @file atm_gap.h
 *
 * @brief Atmosic GAP procedure
 *
 * Copyright (C) Atmosic 2020-2022
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM Atmosic Framework
 * @brief ATM bluetooth framework
 *
 * ATM bluetooth framework components.
 *
 * @defgroup ATM_BTFM_PROC Framework Procedure
 * @ingroup ATM_BTFM
 * @brief ATM bluetooth procedure
 * Procedure layer which on top of API.
 *
 * @defgroup ATM_BTFM_API Framework Interface(API)
 * @ingroup ATM_BTFM
 * @brief ATM bluetooth API layer.
 *
 *******************************************************************************
 */

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_GAP GAP procedures
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework GAP procedures
 *
 * This module contains the necessary procedure to run the platform with the ATM
 * bluetooth framework API.
 *
 * @{
 *******************************************************************************
 */

#include "ble_gap.h"
#include "gapc_task.h"

#ifdef __cplusplus
extern "C" {
#endif

/// constant parameter
#ifndef CFG_GAP_PARAM_CONST
#define CFG_GAP_PARAM_CONST 1
#endif

#if CFG_GAP_PARAM_CONST
#define __ATM_GAP_PARAM_CONST const
#else
#define __ATM_GAP_PARAM_CONST
#endif

/// Invalid activity index
#define ATM_INVALID_ACTIDX 0xFF

/// Indicate that a connection has been established
typedef ble_gap_ind_con_est_t atm_connect_info_t;

/// Link states
typedef enum {
    /// Link is closed.
    LINK_CLOSED,
    /// Link is opened but not adopted yet.
    LINK_CONNECTING,
    /// Link is opened
    LINK_CONNECTED,
    /// Link is closing
    LINK_CLOSING,
} atm_link_state_t;

/// GAP appearance.
/// Sourced from: https://specificationrefs.bluetooth.com/assigned-values/
/// Appearance Values.pdf
typedef enum {
    /// Generic Unknown
    ATM_GAP_APPEARANCE_UNKNOWN = 0,
    /// Generic Phone
    ATM_GAP_APPEARANCE_GENERIC_PHONE = 64,
    /// Generic Computer
    ATM_GAP_APPEARANCE_GENERIC_COMPUTER = 128,
    /// Generic Watch
    ATM_GAP_APPEARANCE_GENERIC_WATCH = 192,
    /// Sports Watch
    ATM_GAP_APPEARANCE_WATCH_SPORTS_WATCH = 193,
    /// Generic Clock
    ATM_GAP_APPEARANCE_GENERIC_CLOCK = 256,
    /// Generic Display
    ATM_GAP_APPEARANCE_GENERIC_DISPLAY = 320,
    /// Generic  Remote Control
    ATM_GAP_APPEARANCE_GENERIC_REMOTE_CONTROL = 384,
    /// Generic  Eye-glasses
    ATM_GAP_APPEARANCE_GENERIC_EYE_GLASSES = 448,
    /// Generic Tag
    ATM_GAP_APPEARANCE_GENERIC_TAG = 512,
    /// Generic Keyring
    ATM_GAP_APPEARANCE_GENERIC_KEYRING = 576,
    /// Generic Media Player
    ATM_GAP_APPEARANCE_GENERIC_MEDIA_PLAYER = 640,
    /// Generic Barcode Scanner
    ATM_GAP_APPEARANCE_GENERIC_BARCODE_SCANNER = 704,
    /// Generic Thermometer
    ATM_GAP_APPEARANCE_GENERIC_THERMOMETER = 768,
    /// Ear Thermometer
    ATM_GAP_APPEARANCE_THERMOMETER_EAR = 769,
    /// Generic Heart Rate Sensor
    ATM_GAP_APPEARANCE_GENERIC_HEART_RATE_SENSOR = 832,
    /// Heart Rate Belt
    ATM_GAP_APPEARANCE_HEART_RATE_SENSOR_HEART_RATE_BELT = 833,
    /// Generic Blood Pressure
    ATM_GAP_APPEARANCE_GENERIC_BLOOD_PRESSURE = 896,
    /// Arm Blood Pressure
    ATM_GAP_APPEARANCE_BLOOD_PRESSURE_ARM = 897,
    /// Wrist Blood Pressure
    ATM_GAP_APPEARANCE_BLOOD_PRESSURE_WRIST = 898,
    /// Generic Human Interface Device
    ATM_GAP_APPEARANCE_HUMAN_INTERFACE_DEVICE = 960,
    /// Keyboard
    ATM_GAP_APPEARANCE_KEYBOARD = 961,
    /// Mouse
    ATM_GAP_APPEARANCE_MOUSE = 962,
    /// Joystick
    ATM_GAP_APPEARANCE_JOYSTICK = 963,
    /// Gamepad
    ATM_GAP_APPEARANCE_GAMEPAD = 964,
    /// Digitizer Tablet
    ATM_GAP_APPEARANCE_DIGITIZER_TABLET = 965,
    /// Card Reader
    ATM_GAP_APPEARANCE_CARD_READER = 966,
    /// Digital Pen
    ATM_GAP_APPEARANCE_DIGITAL_PEN = 967,
    /// Barcode Scanner
    ATM_GAP_APPEARANCE_BARCODE_SCANNER = 968,
    /// Generic Glucose Meter
    ATM_GAP_APPEARANCE_GENERIC_GLUCOSE_METER = 1024,
    /// Generic Running Walking Sensor
    ATM_GAP_APPEARANCE_GENERIC_RUNNING_WALKING_SENSOR = 1088,
    /// In-Shoe Running Walking Sensor
    ATM_GAP_APPEARANCE_RUNNING_WALKING_SENSOR_IN_SHOE = 1089,
    /// On-Shoe Running Walking Sensor
    ATM_GAP_APPEARANCE_RUNNING_WALKING_SENSOR_ON_SHOE = 1090,
    /// On-Hip Running Walking Sensor
    ATM_GAP_APPEARANCE_RUNNING_WALKING_SENSOR_ON_HIP = 1091,
    /// Generic Cycling
    ATM_GAP_APPEARANCE_GENERIC_CYCLING = 1152,
    /// Cycling Computer
    ATM_GAP_APPEARANCE_CYCLING_CYCLING_COMPUTER = 1153,
    /// Speed Sensor
    ATM_GAP_APPEARANCE_CYCLING_SPEED_SENSOR = 1154,
    /// Cadence Sensor
    ATM_GAP_APPEARANCE_CYCLING_CADENCE_SENSOR = 1155,
    /// Power Sensor
    ATM_GAP_APPEARANCE_CYCLING_POWER_SENSOR = 1156,
    /// Speed and Cadence Sensor
    ATM_GAP_APPEARANCE_CYCLING_SPEED_AND_CADENCE_SENSOR = 1157,
    /// Generic Pulse Oximeter
    ATM_GAP_APPEARANCE_GENERIC_PULSE_OXIMETER = 3136,
    /// Fingertip Pulse Oximeter
    ATM_GAP_APPEARANCE_FINGERTIP = 3137,
    /// Wrist Worn Pulse Oximeter
    ATM_GAP_APPEARANCE_WRIST_WORN = 3138,
    /// Generic Weight Scale
    ATM_GAP_APPEARANCE_GENERIC_WEIGHT_SCALE = 3200,
    /// Generic Personal Mobility Device
    ATM_GAP_APPEARANCE_GENERIC_PERSONAL_MOBILITY_DEVICE = 3264,
    /// Powered Wheelchair
    ATM_GAP_APPEARANCE_POWERED_WHEELCHAIR = 3265,
    /// Mobility Scooter
    ATM_GAP_APPEARANCE_MOBILITY_SCOOTER = 3266,
    /// Generic Continuous Glucose Monitor
    ATM_GAP_APPEARANCE_GENERIC_CONTINUOUS_GLUCOSE_MONITOR = 3328,
    /// Generic Insulin Pump
    ATM_GAP_APPEARANCE_GENERIC_INSULIN_PUMP = 3392,
    /// nsulin Pump, durable pump
    ATM_GAP_APPEARANCE_INSULIN_PUMP_DURABLE_PUMP = 3393,
    /// Insulin Pump, patch pump
    ATM_GAP_APPEARANCE_INSULIN_PUMP_PATCH_PUMP = 3396,
    /// Insulin Pen
    ATM_GAP_APPEARANCE_INSULIN_PEN = 3400,
    /// Generic Medication Delivery
    ATM_GAP_APPEARANCE_GENERIC_MEDICATION_DELIVERY = 3456,
    /// Generic Outdoor Sports Activity
    ATM_GAP_APPEARANCE_GENERIC_OUTDOOR_SPORTS_ACTIVITY = 5184,
    /// Location Display
    ATM_GAP_APPEARANCE_LOCATION_DISPLAY_DEVICE = 5185,
    /// Location and Navigation Display
    ATM_GAP_APPEARANCE_LOCATION_AND_NAVIGATION_DISPLAY_DEVICE = 5186,
    /// Location Pod
    ATM_GAP_APPEARANCE_LOCATION_POD = 5187,
    /// Location and Navigation Pod
    ATM_GAP_APPEARANCE_LOCATION_AND_NAVIGATION_POD = 5188
} atm_gap_appearance_t;

/// ATM command complete callback prototype
/// @param[in] conidx Connection index.
/// @param[in] status Command status.
typedef void (*atm_gap_cmd_cb)(uint8_t conidx, ble_err_code_t status);

/// GAP initialization parameters
typedef struct {
    /// Device name string array. If NVDS exist, it will be overwritten.
    uint8_t __ATM_GAP_PARAM_CONST *dev_name;
    /// Length of dev_name.
    uint8_t dev_name_len;
    /// Device name array size.
    uint8_t dev_name_max;
    /// Device Appearance Icon
    atm_gap_appearance_t appearance;
    /// Slave preferred connection parameters
    ble_gap_periph_pref_t periph_pref_params;
    /// IRK assignment from application. Null if not used.
    uint8_t const *app_irk;
    /// Set device configuration
    ble_gap_set_dev_config_t __ATM_GAP_PARAM_CONST *dev_config;
    /// Own static random address
    ble_bdaddr_t addr;
} atm_gap_param_t;

/// GAP application callbacks
typedef struct {
    /// Advertising report indication.
    /// @param[in] ind Report information.
    void (*ext_adv_ind)(ble_gap_ind_ext_adv_report_t const *ind);
    /// Connection indication
    /// @brief This function will be called after ACL connected.
    /// Application should call @ref atm_gap_connect_accept or
    /// @ref atm_gap_disconnect.
    /// @param[in] conidx Connection index.
    /// @param[in] param Connection parameters.
    void (*conn_ind)(uint8_t conidx, atm_connect_info_t *param);
    /// Disconnection indication
    /// @brief This function will be called after ACL disconnected.
    /// @param[in] conidx Connection index.
    /// @param[in] param Disconnection information.
    void (*disc_ind)(uint8_t conidx, ble_gap_ind_discon_t const *param);
    /// Pairing request indication
    /// @brief Pairing information request by pairing procedure.
    /// After receiving this, application should call
    /// @ref ble_gap_sec_pairing_rsp.
    /// @param[in] conidx Connection index.
    /// @param[in] auth_req Authentication request.
    void (*pair_req_ind)(uint8_t conidx, enum gap_auth auth_req);
    /// Security request indication
    /// @brief Security request by slave.
    /// @param[in] conidx Connection index.
    /// @param[in] auth_req Authentication request.
    void (*sec_req_ind)(uint8_t conidx, enum gap_auth auth_req);
    /// Passkey indication
    /// @brief Indication of passkey display or passkey input
    /// After received this indication, application shall show number to user
    /// (GAP_TK_DISPLAY)
    /// or get input(GAP_TK_KEY_ENTRY) from user and call
    /// @ref ble_gap_sec_passkey_rsp.
    /// @param[in] type Decide display(GAP_TK_DISPLAY) or
    /// input(GAP_TK_KEY_ENTRY, GAP_TK_KEY_OOB)
    /// @param[in] conidx Connection index.
    /// @param[in] passkey Random number generated by lower layer. It is used
    /// when type is @ref GAP_TK_DISPLAY.
    /// Application must show it to user and pass it as argument of
    /// ble_gap_sec_passkey_rsp. If application want
    /// to display with other number, then argument to ble_gap_sec_passkey_rsp
    /// have to change as well.
    void (*pair_passkey_ind)(enum gap_tk_type type, uint8_t conidx,
	uint32_t passkey);
    /// Numeric comparison indication
    /// @brief Indication of numeric comparison received.
    /// @param[in] conidx Connection index.
    /// @param[in] number Number generated between pairing. This shall be
    /// shown to user.
    void (*pair_numeric_ind)(uint8_t conidx, uint32_t number);
    /// Pairing result indication
    /// @brief This function will be called after pairing finished.
    /// @param[in] conidx Connection index.
    /// @param[in] param Bonding information.
    /// @note Link is encrypted when pairing is success.
    void (*pair_ind)(uint8_t conidx, ble_gap_ind_le_pair_end_t const *param);
    /// Connection parameter updated indication
    /// @brief This function will be called when connection parameter changed
    /// @param[in] conidx Connection index.
    void (*conn_param_updated_ind)(uint8_t conidx);
    /// Initialization confirmation.
    /// @brief This function will be called when @ref atm_gap_start finished.
    /// @param[in] status Status for @ref atm_gap_start.
    void (*init_cfm)(ble_err_code_t status);
    /// PHY update indication.
    /// @brief This function will be called after PHY update.
    /// @param[in] conidx Connection index.
    /// @param[in] param PHY update information.
    void (*phy_ind)(uint8_t conidx, ble_gap_le_phy_t const *param);
    /// Encryption indication
    /// @brief This function will be called after link encrypted.
    /// It is invoked during reconnection if encryption is enabled.
    /// @param[in] conidx Connection task index.
    /// @param[in] param Encryption information.
    /// @note This will not be called when link is enctyped at the time of
    /// pairing.
    void (*enc_ind)(uint8_t conidx, ble_gap_ind_encrypt_t const *param);
    /// Link information indication
    /// @brief This function will be called after @ref atm_gap_get_link_info
    /// execution finished or information had been updated.
    /// @param[in] status Information status.
    /// @param[in] param Information data.
    void (*linfo_ind)(ble_err_code_t status, ble_gap_link_info_t const *param);
    /// Periodic sync established indication.
    /// @param[in] ind sync established information.
    void (*sync_est_ind)(struct gapm_sync_established_ind const *ind);
    /// LE Set Data Length Indication
    /// @param[in] conidx Connection index.
    /// @param[in] ind Size of LE data packets negotiated over BLE Link.
    void (*le_pkt_size_ind)(uint8_t conidx, ble_gap_ind_pkt_size_t const *ind);
} atm_gap_cbs_t;

/// Parameter for connection parameter update process
typedef struct {
    /// Confirmation of connection parameter update process
    /// @param[in] conidx Connection index.
    /// @param[in] status Status of result.
    void (*param_nego_cfm)(uint8_t conidx, ble_err_code_t status);
    /// force retry retry_times no master error happen
    bool force_retry;
    /// Max retries for connection parameter update.
    uint8_t retry_times;
    /// Time(centisec) for checking the parameter result.
    uint16_t check_result;
    /// Time(centisec) for delay start after calling atm_gap_connect_param_nego.
    /// @note this setting could prevent chance of the LLCP collision
    uint16_t delay;
    /// Preferred connection parameter.
    ble_gap_conn_param_t const *target;
} atm_gap_param_nego_t;

/// Parameter for setting PHY.
typedef struct {
    /// Callback of setting PHY.
    atm_gap_cmd_cb cb;
    /// TX PHYs.
    ble_gap_phy_mode_t tx_phy;
    /// RX PHYs.
    ble_gap_phy_mode_t rx_phy;
    /// PHY_Options.
    ble_gap_phy_opt_t phy_opt;
} atm_gap_param_set_phy_t;
/*
 * GLOBAL FUNCTION
 * *****************************************************************************
 */

/**
 *******************************************************************************
 * @brief Register profile
 * @param[in] name String of profile identification.
 * @param[in] parm Configured parameter of profile
 *******************************************************************************
 */
__NONNULL(1)
void atm_gap_prf_reg(char const *name, void const *parm);

/**
 *******************************************************************************
 * @brief Initialization BT system
 * @param[in] init Parameter for initialization.
 * @param[in] cbs Callbacks of application.
 *******************************************************************************
 */
__NONNULL_ALL
void atm_gap_start(atm_gap_param_t __ATM_GAP_PARAM_CONST *init, atm_gap_cbs_t const *cbs);

/**
 *******************************************************************************
 * @brief Get local name
 * @param[out] len Length of name.
 * @return Local name string.
 *******************************************************************************
 */
__NONNULL_ALL
uint8_t __ATM_GAP_PARAM_CONST *atm_gap_get_local_name(uint8_t *len);

/**
 *******************************************************************************
 * @brief Accept incoming connection.
 * @param[in] conidx Connection index
 *******************************************************************************
 */
void atm_gap_connect_accept(uint8_t conidx);

/**
 *******************************************************************************
 * @brief Get connection parameters
 * @param[in] conidx Connection index.
 * @return Connection parameter information.
 *******************************************************************************
 */
atm_connect_info_t *atm_gap_get_connect_info(uint8_t conidx);

/**
 *******************************************************************************
 * @brief Get link state
 * @param[in] conidx Connection index.
 * @return Link state information.
 *******************************************************************************
 */
atm_link_state_t atm_gap_get_connect_state(uint8_t conidx);

/**
 *******************************************************************************
 * @brief Launch process of requesting new connection parameter
 * @param[in] conidx Connection index.
 * @param[in] param Parameter for process.
 * @return True if it is allowed. Otherwise means an existing operation
 * kis ongoing.
 *******************************************************************************
 */
__NONNULL(2)
bool atm_gap_connect_param_nego(uint8_t conidx, atm_gap_param_nego_t const *param);

/**
 *******************************************************************************
 * @brief Disconnect the link
 * @param[in] conidx Connection index.
 * @param[in] reason Reason for disconnection. Only allow error code of
 * BLE_HCI_MODULE.
 *******************************************************************************
 */
void atm_gap_disconnect(uint8_t conidx, ble_err_code_t reason);

/**
 *******************************************************************************
 * @brief Disconnect all links
 *******************************************************************************
 */
void atm_gap_disconnect_all(void);

/**
 *******************************************************************************
 * @brief Set LE phy. mode
 * @param[in] conidx Connection index.
 * @param[in] param Parameter for setting.
 *******************************************************************************
 */
__NONNULL(2)
void atm_gap_set_phy(uint8_t conidx, atm_gap_param_set_phy_t const *param);

/**
 *******************************************************************************
 * @brief Get link information from peer
 * @param[in] conidx Connection index.
 * @param[in] type Information to get.
 *******************************************************************************
 */
void atm_gap_get_link_info(uint8_t conidx, ble_gap_link_info_type_t type);

/**
 *******************************************************************************
 * @brief Generate random address
 * @param[in] addr_type Random address type
 *******************************************************************************
 */
void atm_gap_gen_rand_addr(ble_gapm_rand_addr_type_t addr_type);

/**
 *******************************************************************************
 * @brief Lower slave latency locally
 * @param[in] conidx Connection index.
 * @param[in] latency Target local latency. (in number of connection events)
 *******************************************************************************
 */
void atm_gap_lower_slave_latency_locally(uint8_t conidx, uint16_t latency);

/**
 *******************************************************************************
 * @brief Get random address
 * @return Rand address
 *******************************************************************************
 */
ble_bdaddr_t __ATM_GAP_PARAM_CONST *atm_gap_get_rand_addr(void);

/**
 *******************************************************************************
 * @brief Print connection parameter
 * @param[in] info Connection parameter information
 *******************************************************************************
 */
__NONNULL_ALL
void atm_gap_print_conn_param(atm_connect_info_t *info);

#ifdef __cplusplus
}
#endif

/// @} ATM_BTFM_GAP
