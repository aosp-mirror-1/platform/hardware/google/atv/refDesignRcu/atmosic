/**
 *******************************************************************************
 *
 * @file atm_debug.h
 *
 * @brief Atmosic debug utility
 *
 * Copyright (C) Atmosic 2020-2023
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_PDBG DEBUG utility
 * @ingroup ATM_BTFM_PROC
 * @brief ATM bluetooth framework debug utility
 *
 * This module contains the necessary function for debugging.
 *
 * @{
 *******************************************************************************
 */
#include "arch.h"
#include "atm_log.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * MACRO
 *******************************************************************************
 */

#ifdef CFG_DBG
#define ATM_DEBUG
#endif

/// All attribute off (@see ATM_VT_SGR)
#define ATM_DE_COLOR 0
/// Highlight Block color
#define ATM_GG_BLACK 90
/// Highlight Red color
#define ATM_GG_RED 91
/// Highlight Green color
#define ATM_GG_GREEN 92
/// Highlight Brown color
#define ATM_GG_BROWN 93
/// Highlight Grand foreground blue
#define ATM_GG_BLUE 94
/// Highlight Magenta color
#define ATM_GG_MAGENTA 95
/// Highlight Cyan color
#define ATM_GG_CYAN 96
/// Highlight White color
#define ATM_GG_WHITE 97
/// Background Block color
#define ATM_BG_BLACK 40
/// Background Red color
#define ATM_BG_RED 41
/// Background Green color
#define ATM_BG_GREEN 42
/// Background Brown color
#define ATM_BG_BROWN 43
/// Background Blue color
#define ATM_BG_BLUE 44
/// Background Magenta color
#define ATM_BG_MAGENTA 45
/// Background Cyan color
#define ATM_BG_CYAN 46
/// Background White color
#define ATM_BG_WHITE 47
/// Foreground Block color
#define ATM_FG_BLACK 30
/// Foreground Red color
#define ATM_FG_RED 31
/// Foreground Green color
#define ATM_FG_GREEN 32
/// Foreground Brown color
#define ATM_FG_BROWN 33
/// Foreground Blue color
#define ATM_FG_BLUE 34
/// Foreground Magenta color
#define ATM_FG_MAGENTA 35
/// Foreground Cyan color
#define ATM_FG_CYAN 36
/// Foreground White color
#define ATM_FG_WHITE 37

/// cursor position
#define ATM_VT_CUP(x,y) "\x1b["#x";"#y"H"
/// cursor  up
#define ATM_VT_CUU(x) "\x1B["#x"A"
/// cursor  down
#define ATM_VT_CUD(x) "\x1B["#x"B"
/// cursor  forward
#define ATM_VT_CUF(x) "\x1B["#x"C"
/// cursor  back
#define ATM_VT_CUB(x) "\x1B["#x"D"
/// device status report
#define ATM_VT_DSR "\x1B[6n"
/// save cursor position
#define ATM_VT_SCP "\x1B[6s"
/// restore cursor position
#define ATM_VT_RSP "\x1B[6u"
/// erase display and line
#define ATM_VT_ED "\x1B[2J"
#define ATM_VT_EL "\x1B[K"
/// set graphic rendition
#define ATM_VT_SGR "\x1B[%dm"
/// set modes
#define ATM_VT_SM "\x1B[=%dh"
/// reset modes
#define ATM_VT_RM "\x1B[=%dl"
/// graphic rendition
#define ATM_VT_GR(x) "\x1B["#x"m"
/// set default
#define ATM_VT_DE_COLOR "\x1B[0m"
/// set FG Red color
#define ATM_VT_FG_RED "\x1B[31m"
/// set FG Green color
#define ATM_VT_FG_GREEN "\x1B[32m"
/// set FG CYAN color
#define ATM_VT_FG_CYAN "\x1B[36m"
/// set FG Blue color
#define ATM_VT_FG_BLUE "\x1B[34m"
/// set FG Brown color
#define ATM_VT_FG_BROWN "\x1B[33m"
/// set FG White color
#define ATM_VT_FG_WHITE "\x1B[37m"
/// set FG MAGENTA color
#define ATM_VT_FG_MAGENTA "\x1B[35m"

#define HEADER_COLOR(color, header, str) "\x1B[" STR(color) "m" header ATM_VT_DE_COLOR ": " str
#define STR_COLOR(header, color, str) header ": " "\x1B[" STR(color) "m" str ATM_VT_DE_COLOR

// flag of hex print
typedef enum {
    // all disabled
    atm_debug_hp_none = 0,
    // 16 columns output. If not set, output will be 8 columns.
    atm_debug_hp_16col = (1 << 0),
    // Additional ASCII output enable.
    atm_debug_hp_ascii_en = (1 << 1),
    // Indent enabled. 8 spaces.
    atm_debug_hp_indent_en = (1 << 2),
} atm_debug_hp_flag_t;

/*
 * GLOBAL
 *******************************************************************************
 */
#if (KE_PROFILING)
/**
 *******************************************************************************
 * @brief Get memory usage by memory type
 * @param[in] mem_type memory type
 * @return Used memory size
 *******************************************************************************
 */
uint16_t atm_debug_mem_usage_by_type(enum KE_MEM_HEAP mem_type);
#endif
/**
 *******************************************************************************
 * @brief Get total memory size by memory type
 * @param[in] mem_type memory type
 * @return Total memory size value
 *******************************************************************************
 */
uint16_t atm_debug_mem_total_by_type(enum KE_MEM_HEAP mem_type);

#ifdef ATM_DEBUG
/**
 *******************************************************************************
 * @brief Print string
 * @param[in] header Header string array
 * @param[in] str Content string array
 *******************************************************************************
 */
#define atm_debug_normal(header, str, ...)\
    DEBUG_TRACE(header ": " str, ##__VA_ARGS__)

/**
 *******************************************************************************
 * @brief Print header string with color
 * @param[in] color Color for header
 * @param[in] header Header string array
 * @param[in] str Content string format
 *******************************************************************************
 */
#define atm_debug_header_color(color, header, str, ...)\
    DEBUG_TRACE(HEADER_COLOR(color, header, str), ##__VA_ARGS__)

/**
 *******************************************************************************
 * @brief Print string with color
 * @param[in] color Color for str
 * @param[in] header Header string array
 * @param[in] str Content string array
 *******************************************************************************
 */
#define atm_debug_str_color(header, color, str, ...)\
    DEBUG_TRACE(STR_COLOR(header, color, str), ##__VA_ARGS__)

/**
 *******************************************************************************
 * @brief Print memory usage
 *******************************************************************************
 */
void atm_debug_print_memory_usage(void);

/**
 *******************************************************************************
 * @brief Print hex output
 * @param[in] flag Output type
 * @param[in] data Data
 * @param[in] len Data length
 *******************************************************************************
 */
void atm_debug_hex_print(atm_debug_hp_flag_t flag, void const *data,
    uint16_t len);

#else
#define atm_debug_normal(header, str, ...) DEBUG_TRACE_COND(0, header ": " str, ##__VA_ARGS__)
#define atm_debug_header_color(color, header, str, ...) DEBUG_TRACE_COND(0, HEADER_COLOR(color, header, str), ##__VA_ARGS__)
#define atm_debug_str_color(header, color, str, ...) DEBUG_TRACE_COND(0, STR_COLOR(header, color, str), ##__VA_ARGS__)
#define atm_debug_print_memory_usage()
#define atm_debug_hex_print(flag, data, len)
#endif

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_PDBG

