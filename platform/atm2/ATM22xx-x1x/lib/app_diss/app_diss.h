/**
 *******************************************************************************
 *
 * @file app_diss.h
 *
 * @brief Device Information Service Configuration Setting
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */

#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_APPDISS Device information service for application layer
 * @ingroup ATM_BTFM_API
 * @brief The device information service for application of ATM bluetooth framework
 *
 * This module contains the device information service interface for application layer.
 *
 * @{
 *******************************************************************************
 */
#include "app_config.h"
#include "ble_diss.h"

#ifdef __cplusplus
extern "C" {
#endif

struct diss_param const *app_dis_param(void);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_APPBASS
