/**
 *******************************************************************************
 *
 * @file ble_atmprfs.h
 *
 * @brief Atmosic Profile Server Middleware
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_ATMPRFS Atmosic profile server API
 * @ingroup ATM_BTFM_API
 * @brief ATM bluetooth framework atmprfs API
 *
 * This module contains the necessary API to create service through Atmosic
 * profile server
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDE
 *******************************************************************************
 */
#include "atm_prfs_task.h"
#include "gattc.h"
#include "ble_prf_itf.h"
#include "ble_gattc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINE
 *******************************************************************************
 */
/// The module name to register to SDK framework
#define BLE_ATMPRFS_MODULE_NAME "ble_atmprfs"
/// Invalid ATT index
#define BLE_ATMPRFS_INVALID_ATTIDX 0xFF
/// The maximum serial number of notification or indication
#define BLE_ATMPRFS_NTF_IND_MAX_SN 0xFFFF

/// The element type in crateing profile
typedef enum {
    /// Service
    BLE_ATMPRFS_ATT_SVC,
    /// Characteristic
    BLE_ATMPRFS_ATT_CHAR,
    /// The descriptor base index
    BLE_ATMPRFS_ATT_DESC_BASE = ATT_DESC_CHAR_EXT_PROPERTIES,
    /// CEOD (0x2900)
    BLE_ATMPRFS_ATT_CHAR_EXT_PROP = BLE_ATMPRFS_ATT_DESC_BASE,
    /// CUDD (0x2901)
    BLE_ATMPRFS_ATT_CHAR_USR_DESC,
    /// CCCD (0x2902)
    BLE_ATMPRFS_ATT_CLI_CHAR_CFG,
    /// SCCD (0x2903)
    BLE_ATMPRFS_ATT_SERVER_CHAR_CFG,
    /// SCCD (0x2904)
    BLE_ATMPRFS_ATT_CHAR_PRES_FOMT,
    /// CAFD (0x2905)
    BLE_ATMPRFS_ATT_CHAR_AGGR_FOMT,
    /// The user defined descriptor (128 bit)
    BLE_ATMPRFS_ATT_USER_DESC
} ble_atmprfs_att_type_t;

typedef const ble_prf_cbs_t *(ble_atmprfs_get_cbs_t)(void const *param);

/*
 * STRUCTURE DEFINITION
 ******************************************************************************
 */

/// GATTC read request indicate
typedef uint8_t (*ble_atmprfs_gattc_read_req_ind_t)(uint8_t conidx,
    uint8_t att_idx);
/// GATTC write request indicate
typedef uint8_t (*ble_atmprfs_gattc_write_req_ind_t)(uint8_t conidx,
    uint8_t att_idx, uint8_t const *data, uint16_t size);
/// GATTC write confirmation indicate
typedef void (*ble_atmprfs_gattc_write_cfm_ind_t)(uint8_t conidx,
    uint8_t att_idx);
/// GATTC ATT information request indicate
typedef uint8_t (*ble_atmprfs_gattc_att_info_req_ind_t)(uint8_t conidx,
    uint8_t att_idx, uint16_t *length);

/// The callback function for ble_atmprfs
typedef struct {
    /// GATTC read request indicate
    ble_atmprfs_gattc_read_req_ind_t read_req;
    /// GATTC write request indicate
    ble_atmprfs_gattc_write_req_ind_t write_req;
    /// GATTC write confirmation indicate
    ble_atmprfs_gattc_write_cfm_ind_t write_cfm;
    /// GATTC ATT information request indicate
    ble_atmprfs_gattc_att_info_req_ind_t att_info_req;
} ble_atmprfs_cbs_t;

/*
 * FUNCTION DECLARATION
 ******************************************************************************
 */

/**
 *******************************************************************************
 * @brief Add service
 *
 * @param[in] uuid      Service UUID (128 bit)
 * @param[in] sec_prop  Security property (ble_att.h)
 * @param[in] cb        The callback function of register service
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_svc(const uint8_t uuid[ATT_UUID_128_LEN],
    uint16_t sec_prop, ble_atmprfs_cbs_t const *cb);

/**
 *******************************************************************************
 * @brief Add 16-bit UUID service
 *
 * @param[in] uuid      Service UUID (16 bit)
 * @param[in] sec_prop  Security property (ble_att.h)
 * @param[in] cb        The callback function of register service
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_svc_16(uint16_t uuid, uint16_t sec_prop,
    ble_atmprfs_cbs_t const *cb);

/**
 *******************************************************************************
 * @brief Add characteristic
 *
 * @param[in] uuid      Characteristic UUID (128 bit)
 * @param[in] perm      Permission (ble_att.h)
 * @param[in] max_size  The maximum size of characteristic
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char(const uint8_t uuid[ATT_UUID_128_LEN], uint16_t perm,
    uint16_t max_size);

/**
 *******************************************************************************
 * @brief Add 16-bit UUID characteristic
 *
 * @param[in] uuid      Characteristic UUID (16 bit)
 * @param[in] perm      Permission (ble_att.h)
 * @param[in] max_size  The maximum size of characteristic
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char_16(uint16_t uuid, uint16_t perm,
    uint16_t max_size);

/**
 *******************************************************************************
 * @brief Add characteristic extended properties descriptor (0x2900)
 *
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char_ext_prop(void);

/**
 *******************************************************************************
 * @brief Add characteristic user description descriptor (0x2901)
 *
 * @param[in] perm      Permission (ble_att.h)
 * @param[in] max_size  The maximum size of descriptor
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char_user_desc(uint16_t perm, uint16_t max_size);

/**
 *******************************************************************************
 * @brief Add client characteristic configuration descriptor (0x2902)
 *
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_client_char_cfg(void);

/**
 *******************************************************************************
 * @brief Add server characteristic configuration descriptor (0x2903)
 *
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_server_char_cfg(void);

/**
 *******************************************************************************
 * @brief Add characteristic presentation format descriptor (0x2904)
 *
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char_pres_format(void);

/**
 *******************************************************************************
 * @brief Add characteristic aggregate format descriptor (0x2905)
 *
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_char_aggr_format(uint16_t max_size);

/**
 *******************************************************************************
 * @brief Add user defined descriptor
 *
 * @param[in] uuid      Descriptor UUID (128 bit)
 * @param[in] perm      Permission (ble_att.h)
 * @param[in] max_size  The maximum size of descriptor
 * @return ATT index. Return BLE_ATMPRFS_INVALID_ATTIDX on failure
 *******************************************************************************
 */
uint8_t ble_atmprfs_add_desc_user_defined(const uint8_t uuid[ATT_UUID_128_LEN],
    uint16_t perm, uint16_t max_size);

/**
 *******************************************************************************
 * @brief Active the service database in queue
 *
 * @return Success: ATT_ERR_NO_ERROR
 *******************************************************************************
 */
uint8_t ble_atmprfs_active_svc_db(void);

/**
 *******************************************************************************
 * @brief Apply data to ATT server when get GATTC read request
 *
 * @param[in]  conidx   Connection index
 * @param[in]  att_idx  ATT index
 * @param[out] data     The data will send to remote
 * @param[in]  length   The data length
 * @return Success: ATT_ERR_NO_ERROR
 *******************************************************************************
 */
uint8_t ble_atmprfs_gattc_read_cfm(uint8_t conidx, uint8_t att_idx,
    uint8_t const *data, uint16_t length);

/**
 *******************************************************************************
 * @brief Send notification to ATT client
 *
 * @param[in]  conidx   Connection index
 * @param[in]  att_idx  ATT index
 * @param[out] data     The data will send to remote
 * @param[in]  length   The data length
 * @param[in]  cb       The complete event after sending notification
 * @return Success: ATT_ERR_NO_ERROR
 *******************************************************************************
 */
uint8_t ble_atmprfs_gattc_send_ntf(uint8_t conidx, uint8_t att_idx,
    uint8_t const *data, uint16_t length, ble_gattc_cmpl_cb_t cb);

/**
 *******************************************************************************
 * @brief Send indication to ATT client
 *
 * @param[in]  conidx   Connection index
 * @param[in]  att_idx  ATT index
 * @param[out] data     The data will send to remote
 * @param[in]  length   The data length
 * @param[in]  cb       The complete event after sending indication
 * @return Success: ATT_ERR_NO_ERROR
 *******************************************************************************
 */
uint8_t ble_atmprfs_gattc_send_ind(uint8_t conidx, uint8_t att_idx,
    uint8_t const *data, uint16_t length, ble_gattc_cmpl_cb_t cb);

/**
 *******************************************************************************
 * @brief Retrieve the last sequence number after sending notification or
 * indication
 *
 * @param[in]  conidx   Connection index
 * @return sequence number
 *******************************************************************************
 */
uint16_t ble_atmprfs_get_ni_last_seq_num(uint8_t conidx);

/**
 *******************************************************************************
 * @brief Retrieve ATT handle by index
 *
 * @param[in] att_idx  The ATT index in profile creating.
 * @return ATT handle. Return ATT_INVALID_HDL if handle not exist.
 *******************************************************************************
 */
uint16_t ble_atmprfs_get_att_handle(uint8_t att_idx);

/**
 *******************************************************************************
 * @brief Get ATT index by ATT handle
 *
 * @param[in]  handle   The ATT handle
 * @param[out] att_idx  ATT index to fill
 * @return Success: ATT_ERR_NO_ERROR, Fail: ATT_ERR_INVALID_HANDLE
 *******************************************************************************
 */
uint8_t ble_atmprfs_get_att_idx(uint16_t handle, uint8_t *att_idx);

/**
 *******************************************************************************
 * @brief Apply data to ATT server when get GATTC att info request
 *
 * @param[in]  conidx   Connection index
 * @param[in]  att_idx  ATT index
 * @param[in]  length   Length
 * @return Success: ATT_ERR_NO_ERROR
 *******************************************************************************
 */
uint8_t ble_atmprfs_gattc_att_info_cfm(uint8_t conidx, uint8_t att_idx,
    uint16_t length);

/**
 *******************************************************************************
 * @brief Enable/Disable service visibility during service discovery
 *
 * @param[in] att_idx  ATT index
 * @param[in] set      1 (service is visible) :  0 (service is not visible)
 * @return Success: BLE_ERR_NO_ERROR
 *******************************************************************************
 */
ble_err_code_t ble_atmprfs_svc_visibility_set(uint8_t att_idx, bool set);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_ATMPRFS

