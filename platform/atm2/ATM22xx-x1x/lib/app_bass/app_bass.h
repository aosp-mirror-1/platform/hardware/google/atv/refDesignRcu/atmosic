/**
 *******************************************************************************
 *
 * @file app_bass.h
 *
 * @brief Battery service for application layer
 *
 * Copyright (C) Atmosic 2020
 *
 *******************************************************************************
 */
#pragma once

/**
 *******************************************************************************
 * @defgroup ATM_BTFM_APPBASS Battery service for application layer
 * @ingroup ATM_BTFM_API
 * @brief The battery service for application of ATM bluetooth framework
 *
 * This module contains the battery service interface for application layer.
 *
 * @{
 *******************************************************************************
 */

/*
 * INCLUDE FILE
 *******************************************************************************
 */
#include "ble_bass.h"
#include "gadc.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 *******************************************************************************
 * @brief Retrieve the parameters of battery service
 * return Battery service parameters
 *******************************************************************************
 */
ble_bass_params_t const *app_bass_param(void);

/**
 *******************************************************************************
 * @brief Get battery level
 * return Return the current battery level
 *******************************************************************************
 */
uint8_t app_bass_get_battery_lvl(void);

/**
 *******************************************************************************
 * @brief Send battery level
 * @param[in] lvl Battery level.
 *******************************************************************************
 */
void app_bass_send_battery_lvl(uint8_t lvl);

#ifdef __cplusplus
}
#endif

///@} ATM_BTFM_APPBASS

