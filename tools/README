
This Atmosic SDK needs an external toolchain download for building images
and system configuration in order to access USB serial devices.

### Arm GNU-RM toolchain ###

The following two commands can be used to fetch and install it in the
current tools directory:

wget 'https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.07/gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2'
tar jfx gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2


If the direct link above does not work, find the 10.3-2021.07 version
for Linux 64-bit from this location:

https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads


The toolchain download md5sum is:

b56ae639d9183c340f065ae114a30202  gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2


### J-Link GDB Server ###

Today, the J-Link GDB Server provides the best support for the atm23 and atm33
families of platforms, which are based on the Arm CoreLink SSE-200 Subsystem.

This GDB server can be obtained from https://www.segger.com/downloads/jlink.
Download and install the J-Link Software Pack for your operating system.  The
following shows how to integrate the J-Link Software Pack version V762a in the
SDK for Linux.

$ cd tools
$ tar xzf JLink_Linux_V762a_x86_64.tgz

Refer to a particular platform's examples README file,
e.g. platform/atm23/ATM23xx-x1x/examples/README, on how to run GDB with a
running target using the J-Link GDB Server.


### OpenOCD ###

This Atmosic SDK requires a version of openocd >= 0.11.0.  Many linux
systems provide older versions, so a binary was included with this SDK.
However, special udev and group permissions are required by openocd in
order to access the USB FTDI SWD interface or J-Link OB(ATM3330).  The easiest way to set
these up is to install the openocd version that is supported by the
linux system.

If openocd isn't easily available, the following commands (run from sudo
or su) may be enough to make it work:

# cp openocd/contrib/60-openocd.rules /etc/udev/rules.d/
# groupadd plugdev
# usermod -aG plugdev $USER


### Python 3 and Google Protocol Buffer Version 3 ###

The Atmosic ISP Tool requires Python 3 as well as Google Protocol
Buffers Version 3 (proto3).  After installing Python 3, obtain the
Python package for proto3 using:

pip3 install protobuf

See the platform-specific instructions for installing Python 3 on
macOS and Windows in README.macOS and README.Windows, respectively.

### MCUBoot ###

Building applications with MCUBoot support requires Python 3.6+ and a few
3rd party python libraries.  For installing Python 3, please see
platform-specific instructions.

To install the necessary libraries, please run the following commands from
the top level SDK directory:

pip3 install -r tools/mcuboot_utils/requirements.txt
pip3 install -r contrib/mcuboot/scripts/requirements.txt
